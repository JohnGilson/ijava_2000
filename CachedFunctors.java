//////////////////////////////////////////////////////////////////////
//
// File Name     : CachedFunctors.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/18/1999 21:00:39
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.*;

class CachedFunctors
{
  CachedFunctors()
  {
    _functors = new Hashtable();
  }
  Functor get(List argumentTypes_)
  {
    return((Functor)functors().get(argumentTypes_));
  }
  Functor get(Class[] argumentTypes_)
  {
    if (argumentTypes_ == null) return(null);
    else return(get(Arrays.asList(argumentTypes_)));
  }
  boolean contains(List argumentTypes_)
  {
    return(functors().containsKey(argumentTypes_));
  }
  boolean contains(Class[] argumentTypes_)
  {
    if (argumentTypes_ == null) return(false);
    else return(contains(Arrays.asList(argumentTypes_)));
  }
  void put(List argumentTypes_, Functor functor_)
  {
    functors().put(argumentTypes_, functor_);
  }
  void put(Class[] argumentTypes_, Functor functor_)
  {
    if (argumentTypes_ == null) return;
    else put(Arrays.asList(argumentTypes_), functor_);
  }
  boolean putIfNotContained(List argumentTypes_, Functor functor_)
  {
    if (contains(argumentTypes_)) return(false);
    else
    {
      put(argumentTypes_, functor_);
      return(true);
    }
  }
  boolean putIfNotContained(Class[] argumentTypes_, Functor functor_)
  {
    if (argumentTypes_ == null) return(false);
    else return(putIfNotContained(Arrays.asList(argumentTypes_), functor_));
  }
  public String toString()
  {
    return(toString(0));
  }
  public String toString(int indent_)
  {
    final int ownIndent = 2;
    String indentation = "";
    String ownIndentation = "";
    for(int i = 0; i < indent_; i++)
      indentation += ' ';
    for(int i = 0; i < ownIndent; i++)
      ownIndentation += ' ';
    if (!functors().isEmpty())
    {
      Set set = functors().entrySet();
      Iterator iterator = set.iterator();
      String s = indentation + "Cached\n";
      s += (indentation + "[\n");
      while(iterator.hasNext())
      {
	s += (indentation + ownIndentation + "[\n");
	Map.Entry entry = (Map.Entry)iterator.next();
	s += (indentation + ownIndentation + ownIndentation +
	      "Argument types: ");
	List argTypes = (List)entry.getKey();
	Iterator argTypesIterator = argTypes.iterator();
	while(argTypesIterator.hasNext())
	{
	  s += (Class)argTypesIterator.next();
	  if (argTypesIterator.hasNext()) s += ", ";
	}
	s += '\n';
	Functor f = (Functor)entry.getValue();
	s += (indentation + ownIndentation + ownIndentation +
	      f.typeNameCapitalized() + ": " + f + '\n');
	s += (indentation + ownIndentation + "]\n");
      }
      s += (indentation + ']');
      return(s);
    }
    else return("");
  }
  int size()
  {
    return(functors().size());
  }
  int numberOfFunctors()
  {
    return(size());
  }
  boolean isEmpty()
  {
    return(functors().isEmpty());
  }
  Map functors()
  {
    return(_functors);
  }
  // A key is a collection of Class objects and its value is the Functor object
  // that should be invoked by a call with those argument types.
  final Map _functors;
}
