//////////////////////////////////////////////////////////////////////
//
// File Name     : AllNParameterOrderedFunctors.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/18/1999 20:54:34
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.*;

// Encapsulate all functors of a given name with N parameters defined on a
// class.  Functors are grouped based on those that can be totally ordered.
class AllNParameterOrderedFunctors
{
  AllNParameterOrderedFunctors()
  {
    _orderedFunctors = new Vector();
  }
  AllNParameterOrderedFunctors(AllNParameterOrderedFunctors orderedFunctors_)
  {
    this();
    Iterator i = orderedFunctors_.functors().iterator();
    while(i.hasNext())
      addList(new OrderedFunctors((OrderedFunctors)i.next()));
  }
  AllNParameterOrderedFunctors(Functor[] functors_)
  {
    this();
    add(functors_);
  }
  AllNParameterOrderedFunctors(List orderedFunctors_)
  {
    _orderedFunctors = orderedFunctors_;
  }
  AllNParameterOrderedFunctors(Functor functor_)
  {
    this();
    add(functor_);
  }
  void add(Functor f_)
  {
    Iterator iterator = functors().iterator();
    while(iterator.hasNext())
      if (((OrderedFunctors)iterator.next()).add(f_))
	return;
    addList(new OrderedFunctors(f_));
  }
  void add(Functor[] functors_)
  {
    OrderedFunctors.addAll(functors_, functors());
  }
  void add(List functors_)
  {
    add((Functor[])functors_.toArray());
  }
  static List addAll(Functor[] functors_)
  {
    return(addAll(functors_, new Vector()));
  }
  static List addAll(Functor[] functors_, List allOrderedFunctors_)
  {
    List buckets = bucketByParameterNumber(functors_);
    int numberOfBuckets = buckets.size();
    for(int i = allOrderedFunctors_.size(); i < numberOfBuckets; i++)
      allOrderedFunctors_.add(null);
    for(int i = 0; i < numberOfBuckets; i++)
    {
      List bucket = (List)buckets.get(i);
      if (bucket != null)
      {
	AllNParameterOrderedFunctors allNParameterOrderedFunctors =
	  (AllNParameterOrderedFunctors)allOrderedFunctors_.get(i);
	if (allNParameterOrderedFunctors == null)
	{
	  List newFunctorsList = OrderedFunctors.addAll(bucket);
	  AllNParameterOrderedFunctors newFunctors =
	    new AllNParameterOrderedFunctors(newFunctorsList);
	  allOrderedFunctors_.set(i, newFunctors);
	}
	else
	{
	  OrderedFunctors.addAll(bucket,
				 allNParameterOrderedFunctors.functors());
	}
      }
    }
    return(allOrderedFunctors_);
  }
  static List addAll(List functors_)
  {
    return(addAll((Functor[])functors_.toArray()));
  }
  static List addAll(List functors_, List allOrderedFunctors_)
  {
    return(addAll((Functor[])functors_.toArray(), allOrderedFunctors_));
  }
  static List bucketByParameterNumber(Functor[] functors_)
  {
    Vector buckets = new Vector();
    if (functors_ == null) return(buckets);
    for(int i = 0; i < functors_.length; i++)
    {
      Functor f = functors_[i];
      if (f != null)
      {
	int parameterNumber = f.getParameterTypes().length;
	if (buckets.size() <= parameterNumber)
	{
	  buckets.setSize(parameterNumber + 1);
	}
	List bucket = (List)buckets.get(parameterNumber);
	if (bucket == null)
	{
	  bucket = new Vector();
	  buckets.set(parameterNumber, bucket);
	}
	bucket.add(f);
      }
    }
    return(buckets);
  }
  static List bucketByParameterNumber(List functors_)
  {
    return(bucketByParameterNumber((Functor[])functors_.toArray()));
  }
  List orderFunctors(List functors_)
  {
    if (functors_ == null || functors_.isEmpty() || functors_.size() == 1)
      return(functors_);
    List orderedFunctors = OrderedFunctors.addAll(functors_);
    List result = new Vector();
    Iterator i = orderedFunctors.iterator();
    while(i.hasNext())
    {
      OrderedFunctors of = (OrderedFunctors)i.next();
      result.add(of.getMostSpecific());
    }
    return(result);
  }
  List get(Class[] argumentTypes_)
  {
    List result = new Vector();
    Iterator i = functors().iterator();
    while(i.hasNext())
    {
      Functor f = ((OrderedFunctors)i.next()).get(argumentTypes_);
      if (f != null) result.add(f);
    }
    return(orderFunctors(result));
  }
  List get(Object[] arguments_)
  {
    List result = new Vector();
    Iterator i = functors().iterator();
    while(i.hasNext())
    {
      Functor f = ((OrderedFunctors)i.next()).get(arguments_);
      if (f != null) result.add(f);
    }
    return(orderFunctors(result));
  }
  List getFromArgClasses(List argumentTypes_)
  {
    List result = new Vector();
    Iterator i = functors().iterator();
    while(i.hasNext())
    {
      Functor f =
	((OrderedFunctors)i.next()).getFromArgClasses(argumentTypes_);
      if (f != null) result.add(f);
    }
    return(orderFunctors(result));
  }
  List getFromArgs(List arguments_)
  {
    List result = new Vector();
    Iterator i = functors().iterator();
    while(i.hasNext())
    {
      Functor f = ((OrderedFunctors)i.next()).getFromArgs(arguments_);
      if (f != null) result.add(f);
    }
    return(orderFunctors(result));
  }
  List getSuper(Class objectType_, Class[] argumentTypes_)
  {
    List result = new Vector();
    Iterator i = functors().iterator();
    while(i.hasNext())
    {
      Functor f =
	((OrderedFunctors)i.next()).getSuper(objectType_, argumentTypes_);
      if (f != null) result.add(f);
    }
    return(orderFunctors(result));
  }
  List getSuper(Class objectType_, Object[] arguments_)
  {
    List result = new Vector();
    Iterator i = functors().iterator();
    while(i.hasNext())
    {
      Functor f =
	((OrderedFunctors)i.next()).getSuper(objectType_, arguments_);
      if (f != null) result.add(f);
    }
    return(orderFunctors(result));
  }
  List getSuperFromArgClasses(Class objectType_, List argumentTypes_)
  {
    List result = new Vector();
    Iterator i = functors().iterator();
    while(i.hasNext())
    {
      Functor f =
	((OrderedFunctors)i.next()).getSuperFromArgClasses(objectType_,
							   argumentTypes_);
      if (f != null) result.add(f);
    }
    return(orderFunctors(result));
  }
  List getSuperFromArgs(Class objectType_, List arguments_)
  {
    List result = new Vector();
    Iterator i = functors().iterator();
    while(i.hasNext())
    {
      Functor f =
	((OrderedFunctors)i.next()).getSuperFromArgs(objectType_,
						     arguments_);
      if (f != null) result.add(f);
    }
    return(orderFunctors(result));
  }
  int numberOfFunctors()
  {
    int total = 0;
    for(int i = 0; i < size(); i++)
      total += get(i).numberOfFunctors();
    return(total);
  }
  int size()
  {
    return(functors().size());
  }
  boolean isEmpty()
  {
    return(functors().isEmpty());
  }
  OrderedFunctors get(int i_)
  {
    return((OrderedFunctors)functors().get(i_));
  }
  void addList(OrderedFunctors of_)
  {
    functors().add(of_);
  }
  public String toString()
  {
    return(toString(0));
  }
  public String toString(int indent_)
  {
    final int ownIndent = 2 + indent_;
    String indentation = "";
    for(int i = 0; i < indent_; i++)
      indentation += ' ';
    if (!functors().isEmpty())
    {
      int numberOfSets = size();
      Functor f = get(0).getMostSpecific();
      String name = f.getName();
      int n = f.getParameterTypes().length;
      String s = indentation + numberOfSets + " " +
	((numberOfSets == 1) ? "set" : "sets") +
	" of totally ordered " + f.typeName() + 's' +
	" named " + name + " of " + n + " " +
	((n == 1) ? "parameter" : "parameters") + '\n';
      s += (indentation + "[\n");
      for(int i = 0; i < numberOfSets; i++)
	s += (get(i).toString(ownIndent) + '\n');
      s += (indentation + ']');
      return(s);
    }
    else return("");
  }
  List functors()
  {
    return(_orderedFunctors);
  }
  // Each element of the collection is an object of ordered functors of
  // N parameters, where N is the same for all elements of the collection.
  final List _orderedFunctors;
}
