//////////////////////////////////////////////////////////////////////
//
// File Name     : LongStandIn.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/19/1999 15:26:13
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

public class LongStandIn extends PrimitiveStandIn
{
  public LongStandIn(long l_)
  {
    _long = new Long(l_);
  }
  public Object asObject()
  {
    return(_long);
  }
  public Class primitiveClass()
  {
    return(long.class);
  }
  Long _long;
}
