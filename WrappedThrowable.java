//////////////////////////////////////////////////////////////////////
//
// File Name     : WrappedThrowable.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 4/20/2000 19:55:46
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

// An exception that wraps another exception.  Overrides the toString method
// to also include the String representation of the wrapped exception.

public class WrappedThrowable extends Throwable
{
  public WrappedThrowable()
  {
    _throwable = null;
  }
  public WrappedThrowable(String msg_)
  {
    super(msg_);
    _throwable = null;
  }
  public WrappedThrowable(Throwable throwable_)
  {
    _throwable = throwable_;
  }
  public WrappedThrowable(Throwable throwable_, String msg_)
  {
    super(msg_);
    _throwable = throwable_;
  }
  public Throwable getWrappedThrowable()
  {
    return(_throwable);
  }
  public String toString()
  {
    String str = super.toString();
    if (getWrappedThrowable() != null)
    {
      str += ": ";
      str += getWrappedThrowable().toString();
    }
    return(str);
  }
  public static void main(String[] args_)
  {
    WrappedThrowable wt1 =
      new WrappedThrowable(new ArrayIndexOutOfBoundsException());
    WrappedThrowable wt2 = new WrappedThrowable
      (new ArrayIndexOutOfBoundsException("Index >= length of array."),
       "Wrapping this exception");
    System.out.println(wt1);
    System.out.println(wt2);
  }
  final protected Throwable _throwable;
}
