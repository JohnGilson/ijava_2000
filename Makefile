INCLUDEPATHS = -I.. -I/usr/local/jdk1.2/include -I/usr/local/jdk1.2/include/solaris
LIBPATHS = -L. -L/usr/local/jdk1.2/jre/lib/sparc -L/usr/local/jdk1.2/jre/lib/sparc/classic -L/usr/local/jdk1.2/jre/lib/sparc/native_threads
RLIBPATHS = -R. -R/usr/local/jdk1.2/jre/lib/sparc -R/usr/local/jdk1.2/jre/lib/sparc/classic -R/usr/local/jdk1.2/jre/lib/sparc/native_threads
LDFLAGS =
DEBUG = -g
OPTIMIZE = -O
SHAREDLIBFLAGS = -G
CCFLAGS = -mt -xcg89 -dalign -KPIC
LIBS = -lsunwjit -ljvm -lhpi -lsunmath -lm

lib := CCFLAGS += $(OPTIMIZE)
libdebug := CCFLAGS += $(DEBUG)

TESTAPP = testiJava
TESTAPPDEBUG = testiJavadebug

testiJava := CCFLAGS += $(OPTIMIZE)
testiJavadebug := CCFLAGS += $(DEBUG)

SRCS= \
Accessibility.C \
ArgsArray.C \
Array.C \
ArrayIndexOutOfBoundsException.C \
ArrayStoreException.C \
ArrayTraits.C \
CArray.C \
CArrayTraits.C \
Class.C \
ClassArray.C \
ClassCastException.C \
ClassOf.C \
CompareTo.C \
Constructors.C \
CString.C \
EqualTo.C \
Exception.C \
Fields.C \
FindClassException.C \
GetFieldException.C \
HashCodeOf.C \
IllegalArgumentException.C \
InvokeException.C \
IOU.C \
IOUCallback.C \
IOUTimed.C \
JThrowable.C \
JVM.C \
Method.C \
MethodArray.C \
Methods.C \
MethodException.C \
Monitor.C \
MultipleMethodsMatchException.C \
NativeMethod.C \
NativeMethodSignatureMismatchException.C \
NativePeers.C \
NoSuchMethodException.C \
NullPointerException.C \
NullTraits.C \
Object.C \
ObjectArray.C \
ObjectFrom.C \
ObjectOwner.C \
Primitive.C \
PrimitiveArray.C \
PrimitiveArrayTraits.C \
ReturnValue.C \
SetFieldException.C \
StandInObjectFrom.C \
StaticInitialize.C \
String.C \
StringArray.C \
StringIndexOutOfBoundsException.C \
Throwable.C \
ToString.C \
Value.C \
ValueArray.C \
Void.C

OBJS=${SRCS:.C=.o}

LIBNAME=iJava
LIB=lib$(LIBNAME)
SHAREDLIB=$(LIB).so
LIBVERSION=1
SHAREDLIBVERSIONED=$(SHAREDLIB).$(LIBVERSION)

# Suffix rules for C++
# The suffix "ts" is for time stamp.
.SUFFIXES: .C .o .so .ts
.C.o:
	$(COMPILE.C) $< $(INCLUDEPATHS)
	touch $(LIB).ts
.ts.so:
	$(CCC) $(SHAREDLIBFLAGS) -o $@.$(LIBVERSION) $(OBJS)
	- rm -f $@
	ln -s $@.$(LIBVERSION) $@

# Targets
all: lib testiJava java
alldebug: libdebug testiJavadebug javadebug
clean: cppclean javaclean
	- rm -f *~

lib: libts .lib.ts

libdebug: libts .lib.ts

libts:
	if [ ! -f $(LIB).ts ]; then \
           touch $(LIB).ts; \
           echo "File $(LIB).ts must be created before $(SHAREDLIB) can be made.  Remake."; \
        fi

.lib.ts: $(OBJS) $(SHAREDLIB)
	touch $@

testiJava: $(TESTAPP).o
	$(LINK.C) -o $@ $(LIBPATHS) $(RLIBPATHS) $(TESTAPP).o -l$(LIBNAME) $(LIBS)

testiJavadebug: $(TESTAPP).o
	$(LINK.C) -o $@ $(LIBPATHS) $(RLIBPATHS) $(TESTAPP).o -l$(LIBNAME) $(LIBS)

cppclean:
	- rm -f .lib.ts $(OBJS) $(SHAREDLIBVERSIONED) $(TESTAPP).o $(TESTAPP) $(TESTAPPDEBUG) core
	CCadmin -clean

# Java
JAVAC = javac # Java compiler
JAVACFLAGSOPTIMIZE = -O
JAVACFLAGSDEBUG = -g
java := JAVACFLAGS = $(JAVACFLAGSOPTIMIZE)
javadebug := JAVACFLAGS = $(JAVACFLAGSDEBUG)
JAVACCLASSPATH = ..
JAVAVM = java # Java interpreter
JAVAVMFLAGS = 
JAVAVMCLASSPATH = ..
JARCMD = jar # Make Java archive files
JARFLAGS = cvf0 # do not compress JAR file

JAVASRCS = \
Accessibility.java \
AllFunctors.java \
AllNParameterOrderedFunctors.java \
BooleanStandIn.java \
ByteStandIn.java \
CPointer.java \
CachedFunctors.java \
CharStandIn.java \
ConstructorFunctor.java \
ConstructorsByClass.java \
ConstructorsOfClass.java \
DoubleStandIn.java \
FieldResolver.java \
FieldResolversByClass.java \
FloatStandIn.java \
Functor.java \
FunctorComparator.java \
FunctorInvocationConversion.java \
GenericFunction.java \
GenericFunctionsByClass.java \
IOU.java \
IOUTimed.java \
IntStandIn.java \
InvocationTargetException.java \
LongStandIn.java \
MethodFunctor.java \
MultipleConstructorsMatchException.java \
MultipleFieldsMatchException.java \
MultipleMethodsMatchException.java \
NoSuchStaticFieldException.java \
NoSuchStaticMethodException.java \
NonAssignableFieldException.java \
Null.java \
OrderedFunctors.java \
Primitive.java \
PrimitiveStandIn.java \
ShortStandIn.java \
Utility.java \
WrappedThrowable.java \
test.java \
test1.java \
testiJava.java

JAVACLASSES=${JAVASRCS:.java=.class}
JAR=iJava.jar

java: javats .java.ts

javadebug: javats .java.ts

javats:
	if [ ! -f $(JAR:.jar=).ts ]; then \
           touch $(JAR:.jar=).ts; \
           echo "File $(JAR:.jar=).ts must be created before $(JAR) can be made.  Remake."; \
        fi

.java.ts: $(JAVACLASSES) $(JAR)
	touch $@

javaclean:
	- rm -f .java.ts *.class $(JAR)

# Suffix rules for Java.
# The suffix "ts" is for time stamp.
.SUFFIXES: .java .java~ .class .jar .ts
# Compile Java source files to class files.
.java.class:
	$(JAVAC) $(JAVACFLAGS) -classpath "$(JAVACCLASSPATH)" $<
	touch $(JAR:.jar=).ts
.java~.class:
	$(GET) $(GFLAGS) -p $< > $*.java
	$(JAVAC) $(JAVACFLAGS) -classpath "$(JAVACCLASSPATH)" $<
	touch $(JAR:.jar=).ts
	rm -f $*.java

# Make JAR file from class files.
.ts.jar:
	cd ..;$(JARCMD) $(JARFLAGS) $@ $*/*.class
	mv -f ../$@ .
