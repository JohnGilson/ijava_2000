#ifndef iJava_ClassCastException_INTERFACE
#define iJava_ClassCastException_INTERFACE

//////////////////////////////////////////////////////////////////////
//
// File Name     : ClassCastException.H
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 1/ 5/2000 19:10:30
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

#include <iJava/Exception.H>

#include <string>

namespace iJava
{

  class ClassCastException : public Exception
  {
    public:
    ClassCastException()
      : Exception(messageDefault(), messageCreated())
    {}
    ClassCastException(const std::string & string_)
      : Exception(messagePreface() + string_, messageCreated())
    {}
    protected:
    ClassCastException(const std::string & message_,
		       const MessageCreated & mc_)
      : Exception(message_, mc_)
    {}
    static const std::string & messageDefault();
  };

} // namespace

#endif
