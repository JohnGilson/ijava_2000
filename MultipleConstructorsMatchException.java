//////////////////////////////////////////////////////////////////////
//
// File Name     : MultipleConstructorsMatchException.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/19/1999 11:40:10
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.*;

public class MultipleConstructorsMatchException extends Exception
{
  public MultipleConstructorsMatchException()
  {
    super(_defaultMessage);
  }
  public MultipleConstructorsMatchException(List constructors_)
  {
    super(toString(_defaultMessage, constructors_));
    _constructors = constructors_;
  }
  public MultipleConstructorsMatchException(String msg_)
  {
    super(msg_);
  }
  public MultipleConstructorsMatchException(String msg_, List constructors_)
  {
    super(toString(msg_, constructors_));
    _constructors = constructors_;
  }
  public List constructors()
  {
    return(_constructors);
  }
  static String toString(String message_, List constructors_)
  {
    if (constructors_ != null)
    {
      String s = message_;
      Iterator i = constructors_.iterator();
      while(i.hasNext())
      {
	s += '\n';
	s += i.next().toString();
      }
      return(s);
    }
    else return(message_);
  }
  public String toString()
  {
    return(toString(_defaultMessage, _constructors));
  }
  List _constructors;
  static final String _defaultMessage = "Multiple constructors match.";
}
