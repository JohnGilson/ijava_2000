//////////////////////////////////////////////////////////////////////
//
// File Name     : IntStandIn.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/19/1999 15:25:07
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

public class IntStandIn extends PrimitiveStandIn
{
  public IntStandIn(int i_)
  {
    _integer = new Integer(i_);
  }
  public Object asObject()
  {
    return(_integer);
  }
  public Class primitiveClass()
  {
    return(int.class);
  }
  Integer _integer;
}
