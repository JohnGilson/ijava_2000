//////////////////////////////////////////////////////////////////////
//
// File Name     : testiJava.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 4/ 3/2000 20:20:35
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

public class testiJava
{
  public int mCaller()
  {
    return(m(10.5f, new int[2], new String("hi")));
  }
  public native int m(float f_, int[] ia_, String s_);
  public double m2Caller(float f_)
  {
    return(m2(f_));
  }
  public native static double m2(int i);
  public native static double m2(float f);
  public void m3()
  {
    throw new ArrayIndexOutOfBoundsException();
  }
  public static void main(String[] args_)
  {
    testiJava obj = new testiJava();
    try
    {
      obj.m3();
    }
    catch(Throwable t_)
    {
      System.out.println(t_);
    }
    try
    {
      java.lang.reflect.Method m = testiJava.class.getMethod("m3", null);
      m.invoke(obj, null);
    }
    catch(Throwable t_)
    {
      System.out.println(t_);
    }
  }
}
