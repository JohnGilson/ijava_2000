//////////////////////////////////////////////////////////////////////
//
// File Name     : ByteStandIn.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/19/1999 15:23:17
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

public class ByteStandIn extends PrimitiveStandIn
{
  public ByteStandIn(byte b_)
  {
    _byte = new Byte(b_);
  }
  public Object asObject()
  {
    return(_byte);
  }
  public Class primitiveClass()
  {
    return(byte.class);
  }
  Byte _byte;
}
