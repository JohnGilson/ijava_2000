//////////////////////////////////////////////////////////////////////
//
// File Name     : NoSuchStaticMethodException.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 10/ 6/1999
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.lang.reflect.*;

public class NoSuchStaticMethodException extends Exception
{
  public NoSuchStaticMethodException()
  {
    super(_defaultMessage);
    _method = null;
  }
  public NoSuchStaticMethodException(Method method_)
  {
    super(_defaultMessage);
    _method = method_;
  }
  public NoSuchStaticMethodException(String msg_)
  {
    super(msg_);
    _method = null;
  }
  public NoSuchStaticMethodException(String msg_, Method method_)
  {
    super(msg_);
    _method = method_;
  }
  public Method method()
  {
    return(_method);
  }
  final Method _method;
  static final String _defaultMessage = "Matching method for call not static.";
}
