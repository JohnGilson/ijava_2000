//////////////////////////////////////////////////////////////////////
//
// File Name     : Functor.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/18/1999 13:23:24
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.lang.reflect.*;

// A functor is a functional object.
// It is used to create an abstraction to encompass both methods and
// constructors.
abstract public class Functor
{
  abstract public String toString();
  abstract public boolean equals(Object o);
  abstract public String getName();
  abstract public Class[] getParameterTypes();
  abstract public Class getDeclaringClass();
  abstract public String typeName();
  abstract public String typeNameCapitalized();
  abstract public Method getMethod();
  abstract public Constructor getConstructor();
  public static Class[] getArgsClasses(Object[] arguments_)
  {
    if (arguments_ == null) return(null);
    Class[] classes = new Class[arguments_.length];
    for(int i = 0; i < arguments_.length; i++)
    {
      if (arguments_[i] == null)
	classes[i] = Null.class;
      else
      {
	Class primitiveClass = Primitive.getClass(arguments_[i]);
	if (primitiveClass == null)
	  classes[i] = arguments_[i].getClass();
	else
	  classes[i] = primitiveClass;
      }
    }
    return(classes);
  }
  public static Object[] getInvokableArgs(Object[] args_)
  {
    if (args_ == null) return(null);
    Object[] invokableArgs = args_;
    for(int i = 0; i < args_.length; i++)
    {
      if (Primitive.is(args_[i]))
      {
	if (invokableArgs == args_)
	{
	  invokableArgs = new Object[args_.length];
	  System.arraycopy(args_, 0, invokableArgs, 0, i);
	}
	invokableArgs[i] = Primitive.toObject(args_[i]);
      }
      else if (invokableArgs != args_) invokableArgs[i] = args_[i];
    }
    return(invokableArgs);
  }
  public static String toString(Class receiver_, String mname_,
				Class[] parameterTypes_, Class returnType_)
  {
    String result = new String();
    result += Utility.toClassNameAsDecl(returnType_);
    result += " ";
    result += toString(receiver_, mname_, parameterTypes_);
    return(result);
  }
  public static String toString(Class receiver_, String mname_,
				Class[] parameterTypes_)
  {
    String result = new String();
    result += Utility.toClassNameAsDecl(receiver_);
    result += ".";
    result += mname_;
    result += "(";
    int counter = 0;
    for(int pcounter = 0; pcounter != parameterTypes_.length;
	pcounter++, counter++)
    {
      if (counter != 0)
	result += ", ";
      result += Utility.toClassNameAsDecl(parameterTypes_[pcounter]);
    }
    result += ")";
    return(result);
  }
  public static String toStringNativeMethodImpl(Class receiver_,
						Class[] parameterTypes_,
						Class returnType_)
  {
    String result = new String();
    result += Utility.toClassNameAsDecl(returnType_);
    result += " <native method impl>(JNIEnv *, ";
    result += Utility.toClassNameAsDecl(receiver_);
    for(int pcounter = 0; pcounter != parameterTypes_.length; pcounter++)
    {
      result += ", ";
      result += Utility.toClassNameAsDecl(parameterTypes_[pcounter]);
    }
    result += ")";
    return(result);
  }
  public static void main(String[] args_)
  {
    System.out.println(toString(Class.class, "getDeclaredMethod",
				new Class[]{String.class, Class[].class},
				Method.class));
    System.out.println(toStringNativeMethodImpl(Object.class,
						new Class[]{int.class,
							    Object[].class},
						void.class));
  }
}
