//////////////////////////////////////////////////////////////////////
//
// File Name     : NativeMethodSignatureMismatchException.C
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 3/20/2000 01:36:45
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

#include <iJava/NativeMethodSignatureMismatchException.H>
#include <iJava/Method.H>
#include <iJava/NativeMethod.H>

namespace iJava
{

  NativeMethodSignatureMismatchException::
  NativeMethodSignatureMismatchException(const Method & jNativeMethod_,
					 const NativeMethodImpl & nmimpl_)
    : MethodException(messagePreface() + "Java native method\n" +
		      Method::toString(jNativeMethod_) + "\n" +
		      "can't be implemented by the native function\n" +
		      Method::toStringNativeMethodImpl
		      (nmimpl_.receiverClass(),
		       nmimpl_.parameterTypes(),
		       nmimpl_.returnType()),
		      messageCreated())
  {}

} // namespace
