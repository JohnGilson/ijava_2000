//////////////////////////////////////////////////////////////////////
//
// File Name     : OrderedFunctors.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/18/1999 14:10:50
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.*;

class OrderedFunctors
{
  OrderedFunctors()
  {
    _functors = new Vector();
  }
  OrderedFunctors(Functor f_)
  {
    this();
    addList(f_);
  }
  OrderedFunctors(OrderedFunctors orderedFunctors_)
  {
    _functors = (Vector)((Vector)orderedFunctors_.functors()).clone();
  }
  boolean add(Functor f_)
  {
    if (f_ == null) return(false);
    if (isEmpty())
    {
      addList(f_);
      return(true);
    }
    else
    {
      // Assume that this functor is not equal to one in the collection.
      ListIterator i = functors().listIterator();
      while(i.hasNext())
      {
	Functor f = (Functor)i.next();
	int order = comparator().order(f_, f);
	if (order == 0) return(false); // not orderable
	if (order < 0)
	{
	  // method is more specific so insert at this point
	  i.previous();
	  i.add(f_);
	  return(true);
	}
      }
      addList(f_);
      return(true);
    }
  }
  static class Functors
  {
    Functors(Functor[] functors_, int number_, byte whatReturned_)
    {
      _functors = functors_;
      _number = number_;
      _whatReturned = whatReturned_;
    }
    Functor[] functors()
    {
      return(_functors);
    }
    int number()
    {
      return(_number);
    }
    byte whatReturned()
    {
      return(_whatReturned);
    }
    boolean isEmpty()
    {
      return(number() == 0);
    }
    public String toString()
    {
      String result = "[";
      if (whatReturned() == ReturnToAdd)
	result += ("Number of functors to add = " + number());
      else
	result += ("Number of functors added = " + number());
      result += ", [";
      for(int i = 0; i < functors().length; i++)
      {
	result += functors()[i];
	if (i != (functors().length - 1)) result += ", ";
      }
      result += "]]";
      return(result);
    }
    final Functor[] _functors;
    final int _number;
    final byte _whatReturned;
  }
  static final byte ReturnToAdd = 0x00;
  static final byte ReturnAdded = 0x01;
  Functors add(Functor[] functors_, byte whatReturned_)
  {
    if (functors_ == null || functors_.length == 0) return(null);
    return(add(functors_, whatReturned_, new Functor[functors_.length]));
  }
  Functors add(Functor[] functors_, byte whatReturned_, Functor[] result_)
  {
    if (functors_ == null || functors_.length == 0) return(null);
    int counter = 0;
    for(int i = 0; i < functors_.length; i++)
    {
      if (functors_[i] == null) continue;
      if (add(functors_[i]))
      {
	if (whatReturned_ == ReturnToAdd)
	{
	  result_[i] = null;
	}
	else
	{
	  result_[i] = functors_[i];
	  counter++;
	}
      }
      else
      {
	if (whatReturned_ == ReturnToAdd)
	{
	  result_[i] = functors_[i];
	  counter++;
	}
	else
	{
	  result_[i] = null;
	}
      }
    }
    return(new Functors(result_, counter, whatReturned_));
  }
  Functors add(List functors_, byte whatReturned_)
  {
    return(add((Functor[])functors_.toArray(), whatReturned_));
  }
  Functors add(List functors_, byte whatReturned_, List result_)
  {
    Functor[] functorsArray = (Functor[])functors_.toArray();
    Functor[] resultArray =
      functors_ == result_ ? functorsArray : (Functor[])result_.toArray();
    return(add(functorsArray, whatReturned_, resultArray));
  }
  static List addAll(Functor[] functors_)
  {
    return(addAll(functors_, new Vector()));
  }
  // To a collection of ordered functor objects, add more functors.  These new
  // functors could add to the existing ordered functor objects or cause new
  // ordered functor objects to be created.
  static List addAll(Functor[] functors_, List nParamOrderedFunctors_)
  {
    if (functors_ == null || functors_.length == 0)
      return(nParamOrderedFunctors_);
    Iterator i = nParamOrderedFunctors_.iterator();
    if (i.hasNext())
    {
      OrderedFunctors ordered = (OrderedFunctors)i.next();
      Functors result = ordered.add(functors_, ReturnToAdd);
      while(i.hasNext() && !result.isEmpty())
      {
	ordered = (OrderedFunctors)i.next();
	result =
	  ordered.add(result.functors(), ReturnToAdd, result.functors());
      }
      while(!result.isEmpty())
      {
	ordered = new OrderedFunctors();
	result =
	  ordered.add(result.functors(), ReturnToAdd, result.functors());
	nParamOrderedFunctors_.add(ordered);
      }
    }
    else
    {
      OrderedFunctors ordered = new OrderedFunctors();
      Functors result = ordered.add(functors_, ReturnToAdd);
      nParamOrderedFunctors_.add(ordered);
      while(!result.isEmpty())
      {
	ordered = new OrderedFunctors();
	result =
	  ordered.add(result.functors(), ReturnToAdd, result.functors());
	nParamOrderedFunctors_.add(ordered);
      }
    }
    return(nParamOrderedFunctors_);
  }
  static List addAll(List functors_)
  {
    Object[] oa = functors_.toArray();
    Functor[] fa = new Functor[oa.length];
    for(int i = 0; i < oa.length; i++)
      fa[i] = (Functor)oa[i];
    return(addAll(fa));
  }
  static List addAll(List functors_, List nParamOrderedFunctors_)
  {
    return(addAll((Functor[])functors_.toArray(), nParamOrderedFunctors_));
  }
  Functor get(Class[] argumentTypes_)
  {
    Iterator position = getPosition(argumentTypes_);
    if (position == null) return(null);
    else return((Functor)position.next());
  }
  Functor get(Object[] arguments_)
  {
    if (arguments_ == null) return(null);
    int length = arguments_.length;
    Class[] classes = new Class[length];
    for(int i = 0; i < length; i++)
      classes[i] = arguments_[i].getClass();
    return(get(classes));
  }
  Functor getFromArgClasses(List argumentTypes_)
  {
    if (argumentTypes_ == null) return(null);
    return(get((Class[])argumentTypes_.toArray()));
  }
  Functor getFromArgs(List arguments_)
  {
    if (arguments_ == null) return(null);
    return(get((Object[])arguments_.toArray()));
  }
  Iterator getPosition(Class[] argumentTypes_)
  {
    if (argumentTypes_ == null || size() == 0) return(null);
    ListIterator i = functors().listIterator();
    if (argumentTypes_.length == 0) return(i);
    while(i.hasNext())
    {
      Functor f = (Functor)i.next();
      int order = comparator().order(argumentTypes_, f);
      if (order < 0)
      {
	// argument types are more specific so best match
	i.previous();
	return(i);
      }
    }
    return(null);
  }
  Functor getSuper(Class objectType_, Class[] argumentTypes_)
  {
    if (objectType_.getSuperclass() == null) return(null);
    Iterator position = getPosition(argumentTypes_);
    if (position == null) return(null);
    while(position.hasNext())
    {
      Functor nextFunctor = (Functor)position.next();
      if (nextFunctor.getDeclaringClass() != objectType_)
	return(nextFunctor);
    }
    return(null);
  }
  Functor getSuper(Class objectType_, Object[] arguments_)
  {
    if (arguments_ == null) return(null);
    int length = arguments_.length;
    Class[] argClasses = new Class[length];
    for(int i = 0; i < length; i++)
      argClasses[i] = arguments_[i].getClass();
    return(getSuper(objectType_, argClasses));
  }
  Functor getSuperFromArgClasses(Class objectType_, List argumentTypes_)
  {
    if (argumentTypes_ == null) return(null);
    return(getSuper(objectType_, (Class[])argumentTypes_.toArray()));
  }
  Functor getSuperFromArgs(Class objectType_, List arguments_)
  {
    if (arguments_ == null) return(null);
    return(getSuper(objectType_, (Object[])arguments_.toArray()));
  }
  Functor getMostSpecific()
  {
    if (isEmpty()) return(null);
    else return(get(getMostSpecificIndex()));
  }
  Functor getLeastSpecific()
  {
    if (isEmpty()) return(null);
    else return(get(getLeastSpecificIndex()));
  }
  int getMostSpecificIndex()
  {
    return(comparator().getMostSpecificIndex(this));
  }
  int getLeastSpecificIndex()
  {
    return(comparator().getNextLeastSpecificIndex(this) - 1);
  }
  int numberOfFunctors()
  {
    return(size());
  }
  int size()
  {
    return(functors().size());
  }
  boolean isEmpty()
  {
    return(functors().isEmpty());
  }
  Functor get(int i_)
  {
    return((Functor)functors().get(i_));
  }
  void addList(Functor f_)
  {
    functors().add(f_);
  }
  void addList(int i_, Functor f_)
  {
    functors().add(i_, f_);
  }
  public String toString()
  {
    return(toString(0));
  }
  public String toString(int indent_)
  {
    final int ownIndent = 2;
    String indentation = "";
    String ownIndentation = "";
    for(int i = 0; i < indent_; i++)
    {
      indentation += ' ';
      ownIndentation += ' ';
    }
    for(int i = 0; i < ownIndent; i++)
      ownIndentation += ' ';
    if (!functors().isEmpty())
    {
      Functor f = getMostSpecific();
      String name = f.getName();
      int n = f.getParameterTypes().length;
      String s = indentation + size() + " totally ordered " +
	((size() == 1) ? f.typeName() : (f.typeName() + 's')) + " named " +
	name + " of " +	n + " " +
	((n == 1) ? "parameter" : "parameters") + '\n';
      s += (indentation + "[\n");
      for(int i = 0; i < size(); i++)
	s += (ownIndentation + get(i) + '\n');
      s += (indentation + ']');
      return(s);
    }
    else return("");
  }
  FunctorComparator comparator()
  {
    return(_comparator);
  }
  List functors()
  {
    return(_functors);
  }
  // A collection of functors that can be ordered, i.e., any functor in the
  // collection can be determined to be more or less specific than any other
  // functor in the collection.
  final List _functors;
  // A comparator object to order a collection of functors.
  static final FunctorComparator _comparator = new FunctorComparator();
}
