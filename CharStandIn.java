//////////////////////////////////////////////////////////////////////
//
// File Name     : CharStandIn.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/19/1999 15:22:20
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

public class CharStandIn extends PrimitiveStandIn
{
  public CharStandIn(char c_)
  {
    _character = new Character(c_);
  }
  public Object asObject()
  {
    return(_character);
  }
  public Class primitiveClass()
  {
    return(char.class);
  }
  Character _character;
}
