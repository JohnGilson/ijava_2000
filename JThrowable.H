#ifndef iJava_JThrowable_INTERFACE
#define iJava_JThrowable_INTERFACE

//////////////////////////////////////////////////////////////////////
//
// File Name     : JThrowable.H
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 1/26/2000 14:47:02
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

#include <iJava/Object.H>
#include <iJava/ClassOf.H>

#include <jni.h>
#include <string>
#include <utility> // std::pair<>

namespace iJava
{

  // Forward references
  class String;
  class Class;
  template<> class ArgsArray<Value>;
  class NullPointerException;
  class ClassCastException;
  class InvokeException;

  class JThrowable : public Object
  {
    typedef Object BaseType;
    public:
    typedef jthrowable JNativeType;
    JThrowable()
    {}
    JThrowable(JNativeType o_)
      : BaseType((BaseType::JNativeType)o_)
    {}
    explicit JThrowable(const Object & o_) throw(ClassCastException);
    operator JNativeType() const
    {
      return(getJValue());
    }
    JNativeType getJValue() const
    {
      return((JNativeType)getJObject());
    }
    String getMessage() const throw(InvokeException, NullPointerException);
    void printStackTrace() const throw(InvokeException, NullPointerException);
    void printStackTrace(const Object & stream_) const
    throw(NullPointerException, ClassCastException, InvokeException);
    const JThrowable & fillInStackTrace() const
    throw(InvokeException, NullPointerException);
    static const JThrowable & null();
    static const std::string & getFullyQualifiedName();
    static const std::string & getClassDescriptor();
    static const std::string & getFieldDescriptor();
    protected:
    JThrowable(JNativeType o_, ReferenceType refType_)
      : BaseType((BaseType::JNativeType)o_, refType_)
    {}
    JThrowable(const std::pair<JNativeType, ReferenceType> & p_)
      : BaseType((const std::pair<BaseType::JNativeType, ReferenceType> &) p_)
    {}
  };

  template<>
    class ClassOf<jthrowable> : public ClassOf<JThrowable>
    {};

} // namespace

#endif
