//////////////////////////////////////////////////////////////////////
//
// File Name     : DoubleStandIn.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/19/1999 15:27:54
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

public class DoubleStandIn extends PrimitiveStandIn
{
  public DoubleStandIn(double d_)
  {
    _double = new Double(d_);
  }
  public Object asObject()
  {
    return(_double);
  }
  public Class primitiveClass()
  {
    return(double.class);
  }
  Double _double;
}
