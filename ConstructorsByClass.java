//////////////////////////////////////////////////////////////////////
//
// File Name     : ConstructorsByClass.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/22/1999 12:45:38
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.*;

public class ConstructorsByClass
{
  public ConstructorsByClass()
  {
    _classToConstructors = new Hashtable();
    _access = new Accessibility();
  }
  public ConstructorsByClass(Accessibility access_)
  {
    _classToConstructors = new Hashtable();
    _access = access_;
  }
  public ConstructorsOfClass get(Class class_)
  {
    return((ConstructorsOfClass)_classToConstructors.get(class_));
  }
  public ConstructorsOfClass get(String name_)
  {
    try
    {
      return((ConstructorsOfClass)
	     _classToConstructors.get(Class.forName(name_)));
    }
    catch(Exception e)
    {
      return(null);
    }
  }
  public boolean contains(Class class_)
  {
    return(get(class_) != null);
  }
  public boolean contains(String name_)
  {
    try
    {
      return(get(Class.forName(name_)) != null);
    }
    catch(Exception e)
    {
      return(false);
    }
  }
  // Put class' constructors if not there.
  public ConstructorsOfClass put(Class class_)
  {
    if (class_ == null) return(null);
    ConstructorsOfClass existing = get(class_);
    if (existing == null)
    {
      ConstructorsOfClass c = new ConstructorsOfClass(class_, _access);
      _classToConstructors.put(class_, c);
      return(c);
    }
    else return(existing);
  }
  public ConstructorsOfClass put(String name_)
  {
    try
    {
      return(put(Class.forName(name_)));
    }
    catch(Exception e)
    {
      return(null);
    }
  }
  public String toString()
  {
    return(toString(0));
  }
  public String toString(int indent_)
  {
    final int ownIndent = 2;
    String indentation = "";
    String ownIndentation = "";
    for(int i = 0; i < indent_; i++)
      indentation += ' ';
    for(int i = 0; i < ownIndent; i++)
      ownIndentation += ' ';
    if (!isEmpty())
    {
      Set classes = _classToConstructors.entrySet();
      Iterator iteratorClasses = classes.iterator();
      String s = indentation + "All constructors by class\n";
      s += (indentation + "[\n");
      while(iteratorClasses.hasNext())
      {
	Map.Entry classEntry = (Map.Entry)iteratorClasses.next();
	String className = ((Class)classEntry.getKey()).getName();
	s += (indentation + ownIndentation + "Class " + className + '\n');
	s += (indentation + ownIndentation + "[\n");
	ConstructorsOfClass c = (ConstructorsOfClass)classEntry.getValue();
	s += c.toString(indent_ + 2 * ownIndent);
	s += '\n';
	s += (indentation + ownIndentation + "]\n");
      }
      s += (indentation + ']');
      return(s);
    }
    else return("");
  }
  public boolean isEmpty()
  {
    return(_classToConstructors.isEmpty());
  }
  public int size()
  {
    return(_classToConstructors.size());
  }
  // Testing
  static class C1
  {
    C1() {}
    C1(C1 c1_) {}
    C1(int i_) {}
  }
  static class C2
  {
    C2() {}
    C2(C1 c1_) {}
    C2(C2 c2_) {}
    C2(int i, float f) {}
    C2(long l, double d) {}
  }
  static void main(String[] args_)
  {
    ConstructorsByClass c = new ConstructorsByClass();
    c.put(C1.class);
    c.put(C2.class);
    c.put(Vector.class);
    System.out.println(c);
  }
  // Each class maps to all of its constructors.
  final Map _classToConstructors;
  final Accessibility _access;
}
