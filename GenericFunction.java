//////////////////////////////////////////////////////////////////////
//
// File Name     : GenericFunction.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 9/29/1999 14:03:48
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.*;
import java.lang.reflect.*;

// In the Common Lisp Object System (CLOS), all the methods of a given name,
// regardless of signature, are collected into an object called a
// generic function.
// This generic function object collects and orders all the methods of a
// given name defined directly on a class or methods defined on a superclass.

public class GenericFunction
{
  public GenericFunction(Class c_, String name_)
  {
    this(c_, name_, new Accessibility());
  }
  public GenericFunction(Class c_, String name_, Accessibility access_)
  {
    _class = c_;
    _name = name_;
    _access = access_;
    _methods = new AllFunctors(getAllMethods());
    _cache = new CachedFunctors();
  }
  public GenericFunction(GenericFunction gf_)
  {
    _class = gf_.receiverClass();
    _name = gf_.name();
    _access = gf_.access();
    _methods = new AllFunctors(gf_.methods());
    _cache = new CachedFunctors();
  }
  // This will construct a generic function that shares methods and the cache
  // with its superclass if possible.
  public GenericFunction(Class c_, GenericFunction superGF_)
  {
    _class = c_;
    _name = superGF_.name();
    _access = superGF_.access();
    Method[] methods = getClassDefinedMethods();
    if (methods == null || methods.length == 0)
    {
      // No new class-specific methods so share generic function.
      _methods = superGF_.methods();
      _cache = superGF_.cache();
    }
    else
    {
      AllFunctors allMethods = new AllFunctors(superGF_.methods());
      allMethods.add(methods);
      _methods = allMethods;
      _cache = new CachedFunctors();
    }
  }
  Method[] getAllMethods()
  {
    if (access().hasPrivate() || access().isUnspecified())
      return(getAllDeclaredMethods());
    Method[] publicMethods = null;
    Method[] packageMethods = null;
    if (access().hasPublic()) publicMethods = getAllPublicMethods();
    if (access().hasPackage()) packageMethods = getAllPackageMethods();
    int allMethodsLength = 0;
    if (publicMethods != null) allMethodsLength += publicMethods.length;
    if (packageMethods != null) allMethodsLength += packageMethods.length;
    Method[] allMethods = null;
    if (allMethodsLength != 0)
    {
      allMethods = new Method[allMethodsLength];
      int publicMethodsLength = 0;
      if (publicMethods != null)
      {
	publicMethodsLength = publicMethods.length;
	System.arraycopy(publicMethods, 0, allMethods, 0, publicMethodsLength);
      }
      if (packageMethods != null)
      {
	System.arraycopy(packageMethods, 0, allMethods, publicMethodsLength,
			 packageMethods.length);
      }
    }
    return(allMethods);
  }
  Method[] getClassDefinedMethods()
  {
    Method[] publicMethods = null;
    Method[] packageMethods = null;
    if (access().hasPublic())
      publicMethods = getClassDefinedPublicMethods();
    if (access().hasPackage())
      packageMethods = getClassDefinedPackageMethods();
    int allMethodsLength = 0;
    if (publicMethods != null) allMethodsLength += publicMethods.length;
    if (packageMethods != null) allMethodsLength += packageMethods.length;
    Method[] allMethods = null;
    if (allMethodsLength != 0)
    {
      allMethods = new Method[allMethodsLength];
      int publicMethodsLength = 0;
      if (publicMethods != null)
      {
	publicMethodsLength = publicMethods.length;
	System.arraycopy(publicMethods, 0, allMethods, 0, publicMethodsLength);
      }
      if (packageMethods != null)
      {
	System.arraycopy(packageMethods, 0, allMethods, publicMethodsLength,
			 packageMethods.length);
      }
    }
    return(allMethods);
  }
  Method[] getAllPublicMethods()
  {
    Method[] allPublic = receiverClass().getMethods();
    return(getMethodsGivenName(allPublic));
  }
  Method[] getClassDefinedPublicMethods()
  {
    Method[] classPublic = receiverClass().getDeclaredMethods();
    return(getMethodsGivenName(classPublic, Accessibility.Public));
  }
  Method[] getAllPackageMethods()
  {
    Methods methods = getAllMethodsWithAccess(Accessibility.Package);
    return(methods == null ? null : methods.get());
  }
  Method[] getClassDefinedPackageMethods()
  {
    Method[] classPackage = receiverClass().getDeclaredMethods();
    return(getMethodsGivenName(classPackage, Accessibility.Package));
  }
  static class Methods
  {
    Methods(int number_)
    {
      _fill = 0;
      _methods = new Method[number_];
    }
    void add(Method method_)
    {
      _methods[_fill++] = method_;
    }
    void add(Method[] methods_)
    {
      if (methods_ != null)
      {
	System.arraycopy(methods_, 0, _methods, _fill, methods_.length);
	_fill += methods_.length;
      }
    }
    Method[] get()
    {
      return(_methods);
    }
    final Method[] _methods;
    int _fill;
  }
  Methods getAllMethodsWithAccess(byte access_)
  {
    return(getAllMethodsWithAccess(receiverClass(), access_, 0));
  }
  Methods getAllMethodsWithAccess(Class c_, byte access_, int n_)
  {
    if (c_ == null)
      return(n_ == 0 ? null : new Methods(n_));
    else
    {
      Method[] allMethods = c_.getDeclaredMethods();
      Method[] accessMethods = getMethodsGivenName(allMethods, access_);
      Class superClass = c_.getSuperclass();
      Methods superClassMethods =
	getAllMethodsWithAccess(superClass, access_,
				accessMethods == null ? n_ :
				n_ + accessMethods.length);
      if (superClassMethods == null) return(null);
      else
      {
	superClassMethods.add(accessMethods);
	return(superClassMethods);
      }
    }
  }
  Method[] getAllDeclaredMethods()
  {
    return(getAllMethodsWithAccess(Accessibility.Private).get());
  }
  Method[] getMethodsGivenName(Method[] methods_)
  {
    return(getMethodsGivenName(methods_, Accessibility.Unspecified));
  }
  Method[] getMethodsGivenName(Method[] methods_, byte access_)
  {
    Method[] allGivenName = new Method[methods_.length];
    int countGivenName = 0;
    for(int counter = 0; counter < methods_.length; counter++)
    {
      Method method = methods_[counter];
      int modifier = method.getModifiers();
      if (method.getName().equals(name()))
      {
	switch(access_)
	{
	case Accessibility.Public:
	  if (Modifier.isPublic(modifier))
	    allGivenName[countGivenName++] = methods_[counter];
	  break;
	case Accessibility.Package:
	  if (Modifier.isProtected(modifier) ||
	      (!Modifier.isPublic(modifier) &&
	       !Modifier.isPrivate(modifier)))
	    allGivenName[countGivenName++] = methods_[counter];
	  break;
	case Accessibility.Unspecified:
	case Accessibility.Private:
	  allGivenName[countGivenName++] = methods_[counter];
	  break;
	default:
	  System.err.println("Error iJava.GenericFunction.getMethodsGivenName(): " +
			     "Unknown access type " + access_ + ".");
	}
      }
    }
    if (countGivenName == 0) return(null);
    else
    {
      Method[] justGivenName = new Method[countGivenName];
      System.arraycopy(allGivenName, 0, justGivenName, 0, countGivenName);
      return(justGivenName);
    }
  }
  public void add(Method method_)
  {
    methods().add(method_);
  }
  public void add(Method[] methods_)
  {
    methods().add(methods_);
  }
  public void add(List methods_)
  {
    methods().add(methods_);
  }
  public Method get(Class[] argumentTypes_)
    throws NoSuchMethodException, MultipleMethodsMatchException
  {
    // Check cache.  If not in cache then find and cache.
    Functor cachedFunctor = cache().get(argumentTypes_);
    if (cachedFunctor == null)
    {
      List matchingFunctors = methods().get(argumentTypes_);
      if (matchingFunctors == null || matchingFunctors.isEmpty())
      {
	String msg = "No match for call " + receiverClass().getName()
	  + "." + name() + "(";
	for(int i = 0; i < argumentTypes_.length; i++)
	{
	  msg += argumentTypes_[i];
	  if (i != argumentTypes_.length - 1) msg += ", ";
	}
	msg += ").";
	throw new NoSuchMethodException(msg);
      }
      if (matchingFunctors.size() > 1)
      {
	Iterator i = matchingFunctors.iterator();
	List matchingMethods = new Vector();
	while(i.hasNext())
	  matchingMethods.add(((Functor)i.next()).getMethod());
	throw new MultipleMethodsMatchException(matchingMethods);
      }
      Functor functor = (Functor)matchingFunctors.get(0);
      Method method = functor.getMethod();
      if (method == null) return(null);
      else
      {
	cache().put(argumentTypes_, functor);
	return(method);
      }
    }
    else return(cachedFunctor.getMethod());
  }
  public Method get(Object[] arguments_)
    throws NoSuchMethodException, MultipleMethodsMatchException
  {
    if (arguments_ == null) return(null);
    Class[] classes = Functor.getArgsClasses(arguments_);
    return(get(classes));
  }
  public Method getFromArgClasses(List argumentTypes_)
    throws NoSuchMethodException, MultipleMethodsMatchException
  {
    return(get((Class[])argumentTypes_.toArray()));
  }
  public Method getFromArgs(List arguments_)
    throws NoSuchMethodException, MultipleMethodsMatchException
  {
    return(get((Object[])arguments_.toArray()));
  }
  public Object invoke(Object object_, Object[] args_)
    throws InvocationTargetException, NoSuchMethodException,
	   MultipleMethodsMatchException, IllegalAccessException
  {
    try
    {
      Method m = get(args_);
      if (m != null)
      {
	if (!m.isAccessible())
	{
	  try
	  {
	    m.setAccessible(true);
	  }
	  catch(SecurityException e_)
	  {
	    throw new IllegalAccessException("Inaccessible method " + m);
	  }
	}
	Object[] invokableArgs = Functor.getInvokableArgs(args_);
	try
	{
	  return(m.invoke(object_, invokableArgs));
	}
	catch(java.lang.reflect.InvocationTargetException e_)
	{
	  throw new InvocationTargetException
	    (e_, "Invoked method " + m + " exception");
	}
      }
    }
    catch(IllegalArgumentException e) {}
    return(null);
  }
  public Object invoke(Object object_, List args_)
    throws InvocationTargetException, NoSuchMethodException,
	   MultipleMethodsMatchException, IllegalAccessException
  {
    return(invoke(object_, (Object[])args_.toArray()));
  }
  public Object invokeStatic(Object object_, Object[] args_)
    throws InvocationTargetException, NoSuchMethodException,
	   MultipleMethodsMatchException, NoSuchStaticMethodException,
	   IllegalAccessException
  {
    // Can pass null as invoking Object.
    try
    {
      Method m = get(args_);
      if (m != null)
      {
	if (!m.isAccessible())
	{
	  try
	  {
	    m.setAccessible(true);
	  }
	  catch(SecurityException e_)
	  {
	    throw new IllegalAccessException("Inaccessible method " + m);
	  }
	}
	Object[] invokableArgs = Functor.getInvokableArgs(args_);
	if (Modifier.isStatic(m.getModifiers()))
	{
	  try
	  {
	    return(m.invoke(object_, invokableArgs));
	  }
	  catch(java.lang.reflect.InvocationTargetException e_)
	  {
	    throw new InvocationTargetException
	      (e_, "Invoked method " + m + " exception");	  }
	}
	else
	{
	  String msg = "Method found but not static for call " +
	    receiverClass().getName() + ".(";
	  for(int i = 0; i < invokableArgs.length; i++)
	  {
	    msg += invokableArgs[i].getClass();
	    if (i != invokableArgs.length - 1) msg += ", ";
	  }
	  msg += ").";
	  throw new NoSuchStaticMethodException(msg, m);
	}
      }
    }
    catch(IllegalArgumentException e) {}
    return(null);
  }
  public Object invokeStatic(Object[] args_)
    throws InvocationTargetException, NoSuchMethodException,
	   MultipleMethodsMatchException, NoSuchStaticMethodException,
	   IllegalAccessException
  {
    return(invokeStatic(null, args_));
  }
  public Object invokeStatic(Object object_, List args_)
    throws InvocationTargetException, NoSuchMethodException,
	   MultipleMethodsMatchException, NoSuchStaticMethodException,
	   IllegalAccessException
  {
    return(invokeStatic(object_, (Object[])args_.toArray()));
  }
  public Object invokeStatic(List args_)
    throws InvocationTargetException, NoSuchMethodException,
	   MultipleMethodsMatchException, NoSuchStaticMethodException,
	   IllegalAccessException
  {
    return(invokeStatic(null, args_));
  }
  public Method getSuper(Class objectType_, Class[] argumentTypes_)
    throws NoSuchMethodException, MultipleMethodsMatchException
  {
    // Check cache.
    Functor cachedFunctor = cache().get(argumentTypes_);
    if (cachedFunctor != null &&
	cachedFunctor.getDeclaringClass() != objectType_)
      return(cachedFunctor.getMethod());
    else
    {
      List matchingFunctors = methods().getSuper(objectType_, argumentTypes_);
      if (matchingFunctors == null || matchingFunctors.isEmpty())
      {
	String msg = "No match for call to super." + name() + "(";
	for(int i = 0; i < argumentTypes_.length; i++)
	{
	  msg += argumentTypes_[i];
	  if (i != argumentTypes_.length - 1) msg += ", ";
	}
	msg += (") from " + receiverClass() + ".");
	throw new NoSuchMethodException(msg);
      }
      if (matchingFunctors.size() > 1)
      {
	Iterator i = matchingFunctors.iterator();
	List matchingMethods = new Vector();
	while(i.hasNext())
	  matchingMethods.add(((Functor)i.next()).getMethod());
	throw new MultipleMethodsMatchException(matchingMethods);
      }
      Functor functor = (Functor)matchingFunctors.get(0);
      Method method = functor.getMethod();
      return(method);
    }
  }
  public Method getSuper(Class objectType_, Object[] arguments_)
    throws NoSuchMethodException, MultipleMethodsMatchException
  {
    if (arguments_ == null) return(null);
    Class[] classes = Functor.getArgsClasses(arguments_);
    return(getSuper(objectType_, classes));
  }
  public Method getSuperFromArgClasses(Class objectType_, List argumentTypes_)
    throws NoSuchMethodException, MultipleMethodsMatchException
  {
    return(getSuper(objectType_, (Class[])argumentTypes_.toArray()));
  }
  public Method getSuperFromArgs(Class objectType_, List arguments_)
    throws NoSuchMethodException, MultipleMethodsMatchException
  {
    return(getSuper(objectType_, (Object[])arguments_.toArray()));
  }
  public Object invokeSuper(Object object_, Object[] args_)
    throws InvocationTargetException, NoSuchMethodException,
	   MultipleMethodsMatchException, IllegalAccessException
  {
    try
    {
      Method m = getSuper(object_.getClass(), args_);
      if (m != null)
      {
	if (!m.isAccessible())
	{
	  try
	  {
	    m.setAccessible(true);
	  }
	  catch(SecurityException e_)
	  {
	    throw new IllegalAccessException("Inaccessible method " + m);
	  }
	}
	Object[] invokableArgs = Functor.getInvokableArgs(args_);
	try
	{
	  return(m.invoke(object_, invokableArgs));
	}
	catch(java.lang.reflect.InvocationTargetException e_)
	{
	  throw new InvocationTargetException
	    (e_, "Invoked method " + m + " exception");
	}
      }
    }
    catch(IllegalArgumentException e) {}
    return(null);
  }
  public Object invokeSuper(Object object_, List args_)
    throws InvocationTargetException, NoSuchMethodException,
	   MultipleMethodsMatchException, IllegalAccessException
  {
    return(invokeSuper(object_, (Object[])args_.toArray()));
  }
  public int numberOfMethods()
  {
    return(methods().numberOfFunctors());
  }
  public int size()
  {
    return(numberOfMethods());
  }
  public boolean isEmpty()
  {
    return(numberOfMethods() == 0);
  }
  public Class receiverClass()
  {
    return(_class);
  }
  public String name()
  {
    return(_name);
  }
  public Accessibility access()
  {
    return(_access);
  }
  AllFunctors methods()
  {
    return(_methods);
  }
  CachedFunctors cache()
  {
    return(_cache);
  }
  public String toString()
  {
    return(toString(0));
  }
  public String toString(int indent_)
  {
    final int ownIndent = 2 + indent_;
    String indentation = "";
    for(int i = 0; i < indent_; i++)
      indentation += ' ';
    String s = indentation + "All " + access() + " methods named " + name() +
      " on " + receiverClass() + '\n';
    s += (indentation + "[\n");
    String methodsString = methods().toString(ownIndent);
    if (!methodsString.equals(""))
    {
      s += methodsString;
      s += '\n';
    }
    String cacheString = cache().toString(ownIndent);
    if (!cacheString.equals(""))
    {
      s += cacheString;
      s += '\n';
    }
    s += (indentation + ']');
    return(s);
  }
  public static Method[] getNativesByClassAndName(Class class_, String name_)
  {
    Method[] dm = class_.getDeclaredMethods();
    List matchingMethods = new Vector();
    for(int i = 0; i != dm.length; i++)
    {
      Method m = dm[i];
      if (Modifier.isNative(m.getModifiers()) && m.getName().equals(name_))
      {
	if (!m.isAccessible())
	{
	  try
	  {
	    m.setAccessible(true);
	  }
	  catch(SecurityException e_)
	  { continue; }
	}
	matchingMethods.add(m);
      }
    }
    int n = matchingMethods.size();
    Method[] ma = new Method[n];
    for(int counter = 0; counter != n; counter++)
      ma[counter] = (Method)matchingMethods.get(counter);
    return(ma);
  }
  public static Method getDeclaredNativeMethod(Class c_, String name_,
					       Class[] pTypes_)
  {
    try
    {
      Method m = c_.getDeclaredMethod(name_, pTypes_);
      boolean isNative = Modifier.isNative(m.getModifiers());
      if (isNative)
	return(m);
    }
    catch(Exception e_) {}
    return(null);
  }
  public static boolean isStatic(Method m_)
  {
    return(Modifier.isStatic(m_.getModifiers()));
  }
  // Testing
  static class C1
  {
    void m(C1 objectC1)
    {
      System.out.println("void C1.m(C1)");
    }
    void m(C2 objectC2)
    {
      System.out.println("void C1.m(C2)");
    }
    void m(int i, double d)
    {
      System.out.println("void C1.m(int, double)");
    }
    void m(double d, int i)
    {
      System.out.println("void C1.m(double, int)");
    }
  }
  static class C2 extends C1
  {
    public void m(int i)
    {
      System.out.println("void C2.m(int)");
    }
    public void m(double d)
    {
      System.out.println("void C2.m(double)");
    }
    void m(C2 objectC2)
    {
      System.out.println("void C2.m(C2)");
    }
    void m(C2 objectC2, double d)
    {
      System.out.println("void C2.m(C2, double)");
    }
  }
  static class C3 extends C2
  {}
  static class C4
  {
    private void m(long l_)
    {
      System.out.println("void C4.m(long)");
    }
    public void m(double d_)
    {
      System.out.println("void C4.m(double)");
    }
  }
  static class C5
  {}
  static class C6 extends C5
  {}
  static class C7 extends C5
  {}
  static class C8 extends C5
  {
    public void m(C5 objectC5)
    {}
    public void m(C6 objectC6)
    {}
    public void m(C8 objectC8)
    {}
  }
  public static void main(String[] args_)
  {
    GenericFunction gfC2m = new GenericFunction(C2.class, "m");
    System.out.println(gfC2m.toString());
    System.out.println();
    System.out.println("**********");
    System.out.println("Get methods named m on class C2");
    try
    {
      System.out.print("C2.m(int): ");
      Method m1 = gfC2m.get(new Class[]{int.class});
      System.out.println(m1);

      System.out.print("C2.m(float): ");
      Method m2 = gfC2m.get(new Class[]{float.class});
      System.out.println(m2);

      System.out.print("C2.m(C2): ");
      Method m3 = gfC2m.get(new Class[]{C2.class});
      System.out.println(m3);

      System.out.print("C2.m(C1): ");
      Method m4 = gfC2m.get(new Class[]{C1.class});
      System.out.println(m4);
      gfC2m.invoke(new C2(), new Object[]{new C1()});

      System.out.println("C2.m(int, int)");
      Method m5 = gfC2m.get(new Class[]{int.class, int.class});
      System.out.println(m5);

      System.out.println("**********");
    }
    catch(Throwable e)
    {
      System.out.println("Exception: " + e.getMessage());
    }
    try
    {
      System.out.println("C2.m(double, double)");
      Method m6 = gfC2m.get(new Class[]{double.class, double.class});
      System.out.println(m6);
    }
    catch(Throwable e)
    {
      System.out.println("Exception: " + e.getMessage());
    }
    try
    {
      gfC2m.invoke(new C2(), new Object[]{new C3(), new FloatStandIn(10.0f)});
      System.out.println("**********");
      System.out.println(gfC2m.toString());
    }
    catch(Throwable e)
    {
      System.out.println("Exception: " + e.getMessage());
    }
    try
    {
      gfC2m.invoke(new C2(), new Object[]{null});
    }
    catch(Throwable e)
    {
      System.out.println("Exception: " + e.getMessage());
    }
    System.out.println("**********");
    GenericFunction gfC4m =
      new GenericFunction(C4.class, "m",
			  new Accessibility(Accessibility.Private));
    System.out.println(gfC4m.toString());
    System.out.println("**********");
    try
    {
      System.out.print("C4.m(int): ");
      Method C4m1 = gfC4m.get(new Class[]{int.class});
      System.out.println(C4m1);
      System.out.println("**********");
      gfC4m.invoke(new C4(), new Object[]{new IntStandIn(123)});
      System.out.println("**********");
    }
    catch(IllegalAccessException e)
    {
      System.out.println("IllegalAccessException: " + e.getMessage());
    }
    catch(Throwable e)
    {
      System.out.println("Exception: " + e.getMessage());
    }
    GenericFunction gfC8m = new GenericFunction(C8.class, "m");
    System.out.println(gfC8m.toString());
    System.out.println("**********");
    System.out.print("C8.m(C8): ");
    try
    {
      Method m7 = gfC8m.get(new Class[]{C8.class});
      System.out.println(m7);
      System.out.println("**********");
    }
    catch(Throwable e)
    {
      System.out.println("Exception: " + e.getMessage());
    }
  }
  final Class _class;
  final String _name;
  final Accessibility _access;
  // The nth element of this collection contains an object with all of the
  // methods of the generic function that have n parameters.
  final AllFunctors _methods;
  final CachedFunctors _cache;
}
