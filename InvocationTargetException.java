//////////////////////////////////////////////////////////////////////
//
// File Name     : InvocationTargetException.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 4/20/2000 22:35:06
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

public class InvocationTargetException extends WrappedThrowable
{
  public InvocationTargetException()
  {}
  public InvocationTargetException(String msg_)
  {
    super(msg_);
  }
  public InvocationTargetException(Throwable throwable_)
  {
    super(throwable_, "Invoked method or constructor exception");
  }
  public InvocationTargetException(Throwable throwable_, String msg_)
  {
    super(throwable_, msg_);
  }
  public InvocationTargetException(java.lang.reflect.InvocationTargetException
				   e_)
  {
    super(e_.getTargetException());
  }
  public InvocationTargetException(java.lang.reflect.InvocationTargetException
				   e_,
				   String msg_)
  {
    super(e_.getTargetException(), msg_);
  }
}
