//////////////////////////////////////////////////////////////////////
//
// File Name     : FieldResolver.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 1/18/2000 14:31:26
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

// Given a reference type (specifically, its Class object), a field name,
// and an access level, resolve to all fields of the given name accessible
// from this type.
// Can resolve to multiple fields because an interface can have
// final static fields, e.g.,
// class C1 { int field1; // instance field }
// interface I1 { String field1 = "hello"; // implicitly final and static }
// class C2 extends C1 implements I1 {}
// C2 obj = new C2();
// obj.field1; // Ambiguous field reference causing a compile-time error
// ((I1)obj).field1; // Refers to the static String field I1.field1
// Resolving to multiple fields or no fields throws an exception.

package iJava;

import java.util.*;
import java.lang.reflect.*;

public class FieldResolver
{
  public FieldResolver()
  {
    _class = null;
    _name = null;
    _access = null;
    _fields = null;
  }
  public FieldResolver(Class c_, String name_)
  {
    this(c_, name_, new Accessibility());
  }
  public FieldResolver(Class c_, String name_, Accessibility access_)
  {
    _class = c_;
    _name = name_;
    _access = access_;
    _fields = getFields();
  }
  // Copy constructor
  public FieldResolver(FieldResolver resolver_)
  {
    if (resolver_ == null)
    {
      _class = null;
      _name = null;
      _access = null;
      _fields = null;
    }
    else
    {
      _class = resolver_.targetClass();
      _name = resolver_.name();
      _access = resolver_.access();
      _fields = resolver_.fields() == null ? null :
	(Field[])resolver_.fields().clone();
    }
  }
  FieldResolver(Class c_, String name_, Accessibility access_, Field field_)
  {
    _class = c_;
    _name = name_;
    _access = access_;
    _fields = new Field[]{field_};
  }
  // Given the FieldResolver objects for all of its immediate supertypes, i.e.,
  // its superclass and any interfaces, construct a FieldResolver object for
  // this class.  An argument flag determines whether the given class is checked
  // for a field of the given name.
  FieldResolver(Class c_, String name_, Accessibility access_,
		FieldResolver[] superResolvers_, boolean checkClass_)
  {
    _class = c_;
    _name = name_;
    _access = access_;
    Field field = null;
    if (checkClass_) field = getClassDefinedField();
    if (field == null)
    {
      // No field of specified name and access level defined in this class.
      if (superResolvers_ != null)
      {
	int length = 0;
	int onlyIndex = 0;
	int number = 0;
	for(int counter = 0; counter < superResolvers_.length; counter++)
	{
	  FieldResolver fr = superResolvers_[counter];
	  if (fr != null && fr.numberOfFields() != 0)
	  {
	    length += fr.numberOfFields();
	    if (number == 0)
	      onlyIndex = counter;
	    number++;
	  }
	}
	if (length == 0)
	  _fields = null;
	else if (number == 1)
	  // share
	  _fields = superResolvers_[onlyIndex].fields();
	else
	{
	  _fields = new Field[length];
	  int fillPointer = 0;
	  for(int counter = 0; counter < superResolvers_.length; counter++)
	  {
	    FieldResolver fr = superResolvers_[counter];
	    if (fr != null && fr.fields() != null)
	    {
	      int sourceLength = fr.numberOfFields();
	      System.arraycopy(fr.fields(), 0, _fields, fillPointer,
			       sourceLength);
	      fillPointer += sourceLength;
	    }
	  }
	}
      }
      else _fields = null;
    }
    else
    {
      _fields = new Field[]{field};
    }
  }
  Field[] getFields()
  {
    Map result = getFields(targetClass(), name(), access());
    if (result == null || result.isEmpty())
      return(null);
    Collection values = result.values();
    Object[] ao = values.toArray();
    Field[] af = new Field[ao.length];
    System.arraycopy(ao, 0, af, 0, ao.length);
    return(af);
  }
  static Map getFields(Class c_, String name_, Accessibility access_)
  {
    if (c_ == null) return(null);
    List classes = new Vector();
    classes.add(c_);
    return(getFields(classes, name_, access_, null));
  }
  // For each path emanating from a root class, traverse the path until a
  // field of the given name is found or the path ends.  Search is
  // breadth-first.
  static Map getFields(List classes_, String name_, Accessibility access_,
		       Map fields_)
  {
    if (classes_ == null || classes_.isEmpty())
      return(fields_);
    Class c = (Class)classes_.remove(0);
    Field f = getClassDefinedField(c, name_, access_);
    if (f != null)
    {
      if (fields_ == null)
	fields_ = new Hashtable();
      if (!fields_.containsKey(c))
	fields_.put(c, f);
      return(getFields(classes_, name_, access_, fields_));
    }
    else
    {
      Class superclass = c.getSuperclass();
      Class[] interfaces = c.getInterfaces();
      if (superclass != null) classes_.add(superclass);
      for(int counter = 0; counter < interfaces.length; counter++)
	classes_.add(interfaces[counter]);
      return(getFields(classes_, name_, access_, fields_));
    }
  }
  static Field
    getClassDefinedField(Class c_, String name_, Accessibility access_)
  {
    try
    {
      // Cannot get the "length" field of an array using this method.
      Field f = c_.getDeclaredField(name_);
      int modifiers = f.getModifiers();
      // Order tests from most to least inclusive
      if (access_.hasPrivate() || access_.isUnspecified())
	return(f);
      else if (access_.hasPackage())
      {
	if (!Modifier.isPrivate(modifiers))
	  return(f);
      }
      else if (access_.hasPublic())
      {
	if (Modifier.isPublic(modifiers))
	  return(f);
      }
    }
    catch(Exception e_)
    {}
    return(null);
  }
  Field getClassDefinedField()
  {
    return(getClassDefinedField(targetClass(), name(), access()));
  }
  public Field get() throws NoSuchFieldException, MultipleFieldsMatchException
  {
    if (isEmpty())
    {
      String msg = "No field named " + name() + " with " + access() +
	" in " + targetClass() + ".";
      throw new NoSuchFieldException(msg);
    }
    else if (numberOfFields() == 1)
      return(fields()[0]);
    else
      throw new MultipleFieldsMatchException(fields());
  }
  public Object getValue(Object target_)
    throws NoSuchFieldException, MultipleFieldsMatchException,
	   IllegalAccessException, NullPointerException, IllegalArgumentException
  {
    if (target_ != null && !targetClass().isAssignableFrom(target_.getClass()))
      throw new IllegalArgumentException("Object is of " + target_.getClass() +
					 " and cannot be cast to " + targetClass() +
					 " to access field " + name() + '.');
    Field f = get();
    if (f != null)
    {
      if (!f.isAccessible())
      {
	try
	{
	  f.setAccessible(true);
	}
	catch(SecurityException e_)
	{
	  throw new IllegalAccessException("Inaccessible field " + f);
	}
      }
      if (target_ == null && !Modifier.isStatic(f.getModifiers()))
	throw new NullPointerException("Object is null and field " +
				       name() + " accessible from " + targetClass() +
				       " is an instance field.");
      return(f.get(target_));
    }
    else return(null);
  }
  public Object setValue(Object target_, Object value_)
    throws NoSuchFieldException, MultipleFieldsMatchException,
	   IllegalAccessException, NullPointerException, NonAssignableFieldException,
	   IllegalArgumentException
  {
    if (target_ != null && !targetClass().isAssignableFrom(target_.getClass()))
      throw new IllegalArgumentException("Object is of " + target_.getClass() +
					 " and cannot be cast to " + targetClass() +
					 " to access field " + name() + '.');
    Field f = get();
    if (f != null)
    {
      if (!f.isAccessible())
      {
	try
	{
	  f.setAccessible(true);
	}
	catch(SecurityException e_)
	{
	  throw new IllegalAccessException("Inaccessible field " + f);
	}
      }
      if (target_ == null && !Modifier.isStatic(f.getModifiers()))
	throw new NullPointerException("Object is null and field " + name() +
				       " accessible from " + targetClass() +
				       " is an instance field.");
      if (Modifier.isFinal(f.getModifiers()))
	throw new NonAssignableFieldException(f);
      f.set(target_, value_);
      return(value_);
    }
    else return(null);
  }
  public Object getValueStatic()
    throws NoSuchFieldException, MultipleFieldsMatchException,
	   IllegalAccessException, NoSuchStaticFieldException
  {
    try
    {
      Field f = get();
      if (f != null)
      {
	if (!f.isAccessible())
	{
	  try
	  {
	    f.setAccessible(true);
	  }
	  catch(SecurityException e_)
	  {
	    throw new IllegalAccessException("Inaccessible field " + f);
	  }
	}
	if (!Modifier.isStatic(f.getModifiers()))
	  throw new NoSuchStaticFieldException("Field " + name() +
					       " accessible from " + targetClass() +
					       " is not static.");
	return(f.get(null));
      }
    }
    catch(IllegalArgumentException e_) {}
    return(null);
  }
  public Object setValueStatic(Object value_)
    throws NoSuchFieldException, MultipleFieldsMatchException,
	   IllegalAccessException, NoSuchStaticFieldException,
	   NonAssignableFieldException, IllegalArgumentException
  {
    Field f = get();
    if (f != null)
    {
      if (!f.isAccessible())
      {
	try
	{
	  f.setAccessible(true);
	}
	catch(SecurityException e_)
	{
	  throw new IllegalAccessException("Inaccessible field " + f);
	}
      }
      if (!Modifier.isStatic(f.getModifiers()))
	throw new NoSuchStaticFieldException("Field " + name() + " accessible from " +
					     targetClass() + " is not static.");
      if (Modifier.isFinal(f.getModifiers()))
	throw new NonAssignableFieldException(f);
      f.set(null, value_);
      return(value_);
    }
    else return(null);
  }
  public int numberOfFields()
  {
    return(fields() == null ? 0 : fields().length);
  }
  public int size()
  {
    return(numberOfFields());
  }
  public boolean isEmpty()
  {
    return(numberOfFields() == 0);
  }
  public Class targetClass()
  {
    return(_class);
  }
  public String name()
  {
    return(_name);
  }
  public Accessibility access()
  {
    return(_access);
  }
  public Field[] fields()
  {
    return(_fields);
  }
  public String toString()
  {
    return(toString(0));
  }
  public String toString(int indent_)
  {
    final int ownIndent = 2 + indent_;
    String indentation = "";
    String ownIndentation = "";
    for(int i = 0; i < indent_; i++)
      indentation += ' ';
    for(int i = 0; i < ownIndent; i++)
      ownIndentation += ' ';
    String s = indentation + "All " + access() + " fields named " + name() +
      " accessible from " + targetClass() + '\n';
    s += (indentation + "[\n");
    Field[] f = fields();
    if (f != null)
      for(int counter = 0; counter < f.length; counter++)
	s += (ownIndentation + f[counter].toString() + '\n');
    s += (indentation + ']');
    return(s);
  }
  // Testing
  static class C1
  {
    int f1C1 = 1;
    String f2C1 = "hello";
  }
  interface I1
  {
    static int f1I1 = 10;
  }
  interface I2 extends I1
  {
    static double f1I1 = 100.0;
  }
  interface I3 extends I1
  {
  }
  static class C2 extends C1 implements I2, I3
  {
    final float f1C1 = 1000.0f;
  }
  public static void main(String[] args_)
  {
    FieldResolver fr1 = new FieldResolver(C2.class, "f1C1");
    FieldResolver fr2 = new FieldResolver(C2.class, "f1I1");
    FieldResolver fr3 = new FieldResolver(C2.class, "f2C1");
    System.out.println(fr1);
    System.out.println(fr2);
    System.out.println(fr3);
    C2 objC2 = new C2();
    try
    {
      System.out.println(fr1.getValue(objC2));
    }
    catch(Exception e_)
    {
      System.out.println(e_.getMessage());
    }
    try
    {
      System.out.println(fr2.getValue(objC2));
    }
    catch(Exception e_)
    {
      System.out.println(e_.getMessage());
    }
    try
    {
      System.out.println(fr3.setValue(objC2, new String("bye")));
    }
    catch(Exception e_)
    {
      System.out.println(e_.getMessage());
    }
    try
    {
      System.out.println(fr3.getValue(objC2));
    }
    catch(Exception e_)
    {
      System.out.println(e_.getMessage());
    }
    try
    {
      System.out.println(fr1.setValue(objC2, new Float(2000.0f)));
    }
    catch(Exception e_)
    {
      System.out.println(e_.getMessage());
    }
  }
  final Class _class;
  final String _name;
  final Accessibility _access;
  final Field[] _fields;
}
