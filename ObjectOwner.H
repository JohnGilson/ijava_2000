#ifndef iJava_ObjectOwner_INTERFACE
#define iJava_ObjectOwner_INTERFACE

//////////////////////////////////////////////////////////////////////
//
// File Name     : ObjectOwner.H
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 5/ 8/2000 14:47:38
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

// Assume a class C that has at least one non-static data member that's an
// iJava object, that is, a wrapper for a Java object reference.  If an
// object, or an array of objects, of class C is heap allocated and therefore
// of indefinite extent, we wish to make the data member references to
// Java objects global references.  This can be done manually using the
// GlobalRef<> class template.  However, this can be easy to overlook.
// By deriving such a class C from the class below, this behavior will occur
// automagically.
// The same holds for class C if it is statically constructed.
// The class Object, the root class for encapsulating Java object references,
// also is derived from the class below to properly handle heap or statically
// allocated Objects and Object arrays.

#include <iJava/ObjectBounds.H>

#include <cstddef> // size_t
#include <new> // operator's std::new, std::new[], std::delete, std::delete[]
#include <iostream>
#include <utility> // std::pair<>

namespace iJava
{

  class ObjectOwner
  {
    public:
    // These operators are, by definition and necessity, static member
    // functions.
    void * operator new(size_t size_) throw(std::bad_alloc);
    void * operator new(size_t size_, const std::nothrow_t &) throw();
    void * operator new(size_t size_, void * ptr_) throw();

    void * operator new[](size_t size_) throw(std::bad_alloc);
    void * operator new[](size_t size_, const std::nothrow_t &) throw();
    void * operator new[](size_t size_, void * ptr_) throw();

    void operator delete(void * p_) throw();
    void operator delete(void * p_, const std::nothrow_t &) throw();
    void operator delete(void * p_, void *) throw();

    void operator delete[](void * ptr_) throw();
    void operator delete[](void * ptr_, const std::nothrow_t &) throw();
    void operator delete[](void * ptr_, void * p_) throw();

    static bool isStaticDataAddr(void * p_);
    static bool isInitializedStaticDataAddr(void * p_);
    static bool isUninitializedStaticDataAddr(void * p_);

    static std::pair<bool, bool> isDynamicDataAddr(void * p_);

    static bool hasIndefiniteExtent(void * p_);

    static void newHandlerInvoked();

    static ObjectBounds::ObjectBoundsType getStaticMemoryBounds();
    static ObjectBounds::ObjectBoundsType getDynamicMemoryHardLimitBounds();
    static int getDynamicMemoryHardLimit();

    static const ObjectBounds & getObjectBounds();

    static std::ostream & write(std::ostream & stream_)
    {
      stream_ << "Static memory: ";
      ObjectBounds::write(stream_, getStaticMemoryBounds());
      stream_ << '\n';
      stream_ << "Dynamic memory (hard limit): ";
      ObjectBounds::write(stream_, getDynamicMemoryHardLimitBounds());
      stream_ << '\n';
      stream_ << "Dynamic memory in use: " << getObjectBounds();
      return(stream_);
    }
    protected:
    static ObjectBounds & setObjectBounds();
    static void * getBreakAddrFromLastNewRequest();

    static bool _newHandlerInvokedFlag;
  };

} // namespace

#endif
