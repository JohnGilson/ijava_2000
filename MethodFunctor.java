//////////////////////////////////////////////////////////////////////
//
// File Name     : MethodFunctor.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/18/1999 20:11:52
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.lang.reflect.*;

class MethodFunctor extends Functor
{
  public MethodFunctor(Method m_)
  {
    _method = m_;
  }
  public String getName()
  {
    return(_method.getName());
  }
  public String typeName()
  {
    return("method");
  }
  public String typeNameCapitalized()
  {
    return("Method");
  }
  public String toString()
  {
    return(_method.toString());
  }
  public Class[] getParameterTypes()
  {
    return(_method.getParameterTypes());
  }
  public Class getDeclaringClass()
  {
    return(_method.getDeclaringClass());
  }
  public boolean equals(Object o)
  {
    return(_method.equals(o));
  }
  public Method getMethod()
  {
    return(_method);
  }
  public Constructor getConstructor()
  {
    return(null);
  }
  public static String toString(Method[] methods_)
  {
    if (methods_ == null) return(null);
    String result = new String();
    int counter = 0;
    for(int mcounter = 0; mcounter != methods_.length; mcounter++, counter++)
    {
      if (counter != 0)
	result += '\n';
      result += methods_[mcounter].toString();
    }
    return(result);
  }
  public static void main(String[] args_)
  {
    try
    {
      Method[] methods = new Method[]
      {Class.class.getDeclaredMethod("getDeclaredMethod",
				     new Class[]{String.class, Class[].class}),
       Class.class.getDeclaredMethod("getDeclaredMethods", null)};
      System.out.println("Methods:\n" + toString(methods));
    }
    catch(Exception e_) {}
  }
  Method _method;
}
