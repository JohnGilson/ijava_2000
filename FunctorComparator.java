//////////////////////////////////////////////////////////////////////
//
// File Name     : FunctorComparator.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/18/1999 16:32:11
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.Comparator;

// A Comparator class to compare two methods to determine which is more
// specific.  The compare method assumes that both methods meet the following
// criteria so an ordering can be determined:
// 1. Same name.
// 2. Same number of parameters.
// 3. One method is more specific than the other (see below).  If one method
//    is not more specific than the other then there is no way to order them
//    and therefore the compare method could not return a legal value.
// The compare method returns:
// 1. A negative integer when the first method is more specific than the
//    second method.
// 2. Zero when the methods are the same.
// 3. A positive integer when the second method is more specific than the
//    first method.
// From the Java Language Specification (Section 15.11.2.2 Choose the Most
// Specific Method):
// Suppose m is the name of a method.
// There are two declarations of methods named m, each having n parameters.
// One declaration appears within a class or interface T and the types of
// the parameters are T1,...,Tn.
// The other declaration appears within a class or interface U and the types
// of the parameters are U1,...,Un.
// The method m declared in T is *more specific* than the method m declared
// in U if and only if both of the following are true:
//   * T can be converted to U by method invocation conversion.
//   * Tj can be converted to Uj by method conversion, for all j from
//     1 to n.

public class FunctorComparator implements Comparator
{
  public int compare(Object o1_, Object o2_)
  {
    if (o1_ instanceof Functor)
    {
      if (o2_ instanceof Functor)
	return(compare((Functor) o1_, (Functor) o2_));
      else return(compare((Functor) o1_, (Class []) o2_));
    }
    if (o2_ instanceof Class[])
      return(compare((Class []) o1_, (Class []) o2_));
    else return(compare((Class []) o1_, (Functor) o2_));
  }
  // Assumes the criteria for comparing two functors has been met.
  public int compare(Functor f1_, Functor f2_)
  {
    if (FunctorInvocationConversion.equals(f1_, f2_)) return(0);
    else if (FunctorInvocationConversion.moreSpecific(f1_, f2_)) return(-1);
    else return(1);
  }
  public int compare(Functor f1_, Class[] types_)
  {
    Class[] parameters = f1_.getParameterTypes();
    if (FunctorInvocationConversion.equals(parameters, types_)) return(0);
    else if (FunctorInvocationConversion.moreSpecific(parameters, types_))
      return(-1);
    else return(1);
  }
  public int compare(Class[] types_, Functor f1_)
  {
    return(-compare(f1_, types_));
  }
  public int compare(Class[] types1_, Class[] types2_)
  {
    if (FunctorInvocationConversion.equals(types1_, types2_)) return(0);
    else if (FunctorInvocationConversion.moreSpecific(types1_, types2_))
      return(-1);
    else return(1);
  }
  // If one method is more specific than, or equal to, the other,
  // then the two methods can be compared and ordered.  When comparing
  // a method to an already-ordered collection of methods, one would
  // compare the new method to each method.  Let's assume a most-specific
  // to least-specific ordering of methods OM and a method m to be compared
  // and, if it can be ordered, added.
  // For each om in OM
  // if m equals om then method already added and break
  // if m is more specific than om then insert at this position and break
  // else if om is more specific than m
  //   if further methods then continue with next method in OM
  //   else add m to end of OM
  // else cannot order m in OM and m should be placed in a new collection

  // An example of unorderable methods:
  /*
     class C1 { public void m(int) {} }
     class C2 extends C1 { public void m(double) {} }

     C1.m() is not more specific than C2.m() because C1 is a superclass of C2.
     C2.m() is not more specific than C1.m() because a double cannot be
     converted to an int by a method invocation conversion.
     Therefore these two methods cannot be ordered.
  */
  public boolean canOrder(Functor f1_, Functor f2_)
  {
    return(order(f1_, f2_) == 0 ? false : true);
  }
  public boolean canOrder(Class[] types1_, Class[] types2_)
  {
    return(order(types1_, types2_) == 0 ? false : true);
  }
  public boolean canOrder(Class[] types1_, Functor f_)
  {
    return(order(types1_, f_) == 0 ? false : true);
  }
  // Determines if the two methods can be ordered and if so then how.
  // Returns:
  // 1. Negative integer if first method is more specific than second method.
  // 2. Positive integer if second method is more specific than first method.
  // 3. Zero if two methods cannot be ordered.
  // If the two methods are the same the ordering doesn't matter.
  public int order(Functor f1_, Functor f2_)
  {
    if (FunctorInvocationConversion.moreSpecific(f1_, f2_)) return(-1);
    if (FunctorInvocationConversion.moreSpecific(f2_, f1_)) return(1);
    else return(0);
  }
  public int order(Class[] types1_, Class[] types2_)
  {
    if (FunctorInvocationConversion.moreSpecific(types1_, types2_)) return(-1);
    if (FunctorInvocationConversion.moreSpecific(types2_, types1_)) return(1);
    else return(0);
  }
  public int order(Class[] types1_, Functor f_)
  {
    Class[] parameterTypes = f_.getParameterTypes();
    return(order(types1_, parameterTypes));
  }
  static int getMostSpecificIndex(OrderedFunctors functors_)
  {
    if (functors_.size() == 0) return(-1);
    else return(0);
  }
  static int getNextLeastSpecificIndex(OrderedFunctors functors_)
  {
    return(functors_.size());
  }
}
