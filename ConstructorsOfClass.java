//////////////////////////////////////////////////////////////////////
//
// File Name     : ConstructorsOfClass.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/18/1999 21:16:27
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.*;
import java.lang.reflect.*;

// Collects and orders all the constructors for a given class.

public class ConstructorsOfClass
{
  public ConstructorsOfClass(Class c_)
  {
    this(c_, new Accessibility());
  }
  public ConstructorsOfClass(Class c_, Accessibility access_)
  {
    _class = c_;
    _access = access_;
    _constructors = new AllFunctors(getAllConstructors());
    _cache = new CachedFunctors();
  }
  // Copy constructor
  public ConstructorsOfClass(ConstructorsOfClass c_)
  {
    _class = c_.cls();
    _access = c_.getAccess();
    _constructors = new AllFunctors(c_.constructors());
    _cache = new CachedFunctors();
  }
  Constructor[] getAllConstructors()
  {
    return(getAllConstructors(cls(), getAccess()));
  }
  static Constructor[] getAllConstructors(Class class_, Accessibility access_)
  {
    if (access_.isUnspecified() || access_.hasPrivate())
      // All declared constructors on class
      return(class_.getDeclaredConstructors());
    else if (access_.hasPackage())
    {
      // All constructors on class except those that are private, i.e.,
      // package-level or greater accessibility.
      Constructor[] constructors = class_.getDeclaredConstructors();
      Constructor[] packageConstructors =
	new Constructor[constructors.length];
      int count = 0;
      for(int counter = 0; counter < constructors.length; counter++)
      {
	Constructor c = constructors[counter];
	int modifier = c.getModifiers();
	if (!Modifier.isPrivate(modifier))
	  packageConstructors[count++] = c;
      }
      if (count != constructors.length)
      {
	Constructor[] temp = packageConstructors;
	packageConstructors = new Constructor[count];
	System.arraycopy(temp, 0, packageConstructors, 0, count);
	return(packageConstructors);
      }
      else return(packageConstructors);
    }
    else if (access_.hasPublic())
      // At this point must only want public constructors
      return(class_.getConstructors());
    else
    {
      System.err.println("Error iJava.ConstructorsOfClass.getAllConstructors(): " +
			 "Unknown access type " + access_ + ".");
      return(null);
    }
  }
  public void add(Constructor constructor_)
  {
    constructors().add(constructor_);
  }
  public void add(Constructor[] constructors_)
  {
    constructors().add(constructors_);
  }
  public void add(List constructors_)
  {
    constructors().add(constructors_);
  }
  public Constructor get(Class[] argumentTypes_)
    throws NoSuchMethodException, MultipleConstructorsMatchException
  {
    // Check cache.  If not in cache then find and cache.
    Functor cachedFunctor = cache().get(argumentTypes_);
    if (cachedFunctor == null)
    {
      List matchingFunctors = constructors().get(argumentTypes_);
      if (matchingFunctors == null || matchingFunctors.isEmpty())
      {
	String msg = "No match for constructor invocation " +
	  cls().getName() + "(";
	for(int i = 0; i < argumentTypes_.length; i++)
	{
	  msg += argumentTypes_[i];
	  if (i != argumentTypes_.length - 1) msg += ", ";
	}
	msg += ").";
	throw new NoSuchMethodException(msg);
      }
      if (matchingFunctors.size() > 1)
      {
	Iterator i = matchingFunctors.iterator();
	List matchingConstructors = new Vector();
	while(i.hasNext())
	  matchingConstructors.add(((Functor)i.next()).getConstructor());
	throw new MultipleConstructorsMatchException(matchingConstructors);
      }
      Functor functor = (Functor)matchingFunctors.get(0);
      Constructor constructor = functor.getConstructor();
      if (constructor == null) return(null);
      else
      {
	cache().put(argumentTypes_, functor);
	return(constructor);
      }
    }
    else return(cachedFunctor.getConstructor());
  }
  public Constructor get(Object[] arguments_)
    throws NoSuchMethodException, MultipleConstructorsMatchException
  {
    if (arguments_ == null) return(null);
    Class[] classes = Functor.getArgsClasses(arguments_);
    return(get(classes));
  }
  public Constructor getFromArgClasses(List argumentTypes_)
    throws NoSuchMethodException, MultipleConstructorsMatchException
  {
    return(get((Class[])argumentTypes_.toArray()));
  }
  public Constructor getFromArgs(List arguments_)
    throws NoSuchMethodException, MultipleConstructorsMatchException
  {
    return(get((Object[])arguments_.toArray()));
  }
  public Object invoke(Object[] args_)
    throws InvocationTargetException, InstantiationException,
	   ExceptionInInitializerError,
	   NoSuchMethodException, MultipleConstructorsMatchException,
	   IllegalAccessException
  {
    try
    {
      Constructor c = get(args_);
      if (c != null)
      {
	if (!c.isAccessible())
	{
	  try
	  {
	    c.setAccessible(true);
	  }
	  catch(SecurityException e_)
	  {
	    throw new IllegalAccessException("Inaccessible constructor " + c);
	  }
	}
	Object[] invokableArgs = Functor.getInvokableArgs(args_);
	try
	{
	  return(c.newInstance(invokableArgs));
	}
	catch(java.lang.reflect.InvocationTargetException e_)
	{
	  throw new InvocationTargetException
	    (e_, "Invoked constructor " + c + " exception");
	}
      }
    }
    catch(IllegalArgumentException e_) {}
    return(null);
  }
  public Object invoke(List args_)
    throws InvocationTargetException, InstantiationException,
	   ExceptionInInitializerError,
	   NoSuchMethodException, MultipleConstructorsMatchException,
	   IllegalAccessException
  {
    return(invoke((Object[])args_.toArray()));
  }
  public int numberOfConstructors()
  {
    return(constructors().numberOfFunctors());
  }
  public int size()
  {
    return(numberOfConstructors());
  }
  public boolean isEmpty()
  {
    return(numberOfConstructors() == 0);
  }
  public Class cls()
  {
    return(_class);
  }
  public Accessibility getAccess()
  {
    return(_access);
  }
  AllFunctors constructors()
  {
    return(_constructors);
  }
  CachedFunctors cache()
  {
    return(_cache);
  }
  public String toString()
  {
    return(toString(0));
  }
  public String toString(int indent_)
  {
    final int ownIndent = 2 + indent_;
    String indentation = "";
    for(int i = 0; i < indent_; i++)
      indentation += ' ';
    String s = indentation + "All " + getAccess() + " constructors on class " +
      cls().getName() + '\n';
    s += (indentation + "[\n");
    String constructorsString = constructors().toString(ownIndent);
    if (!constructorsString.equals(""))
    {
      s += constructorsString;
      s += '\n';
    }
    String cacheString = cache().toString(ownIndent);
    if (!cacheString.equals(""))
    {
      s += cacheString;
      s += '\n';
    }
    s += (indentation + ']');
    return(s);
  }
  // Testing
  static class C1
  {
    public C1()
    {
      System.out.println("C1.C1()");
    }
    C1(int i)
    {
      System.out.println("C1.C1(int i)");
    }
    C1(double d)
    {
      System.out.println("C1.C1(double d)");
    }
    C1(double d, C2 c2)
    {
      System.out.println("C1.C1(double d, C2 c2)");
    }
    C1(float f, C2 c2)
    {
      System.out.println("C1.C1(float f, C2 c2)");
    }
    C1(int i, C3 c3)
    {
      System.out.println("C1.C1(int i, C3 c3)");
    }
    C1(double d, C3 c3)
    {
      System.out.println("C1.C1(double d, C3 c3)");
    }
  }
  static class C2
  {}
  static class C3 extends C2
  {}
  static class C4 extends C3
  {}
  public static void main(String[] args_)
  {
    ConstructorsOfClass c = new ConstructorsOfClass(C1.class);
    System.out.println(c);
    try
    {
      C1 c11 = (C1)c.invoke(new Object[]{new IntStandIn(10)});
      C1 c12 = (C1)c.invoke(new Object[]{new ShortStandIn((short)10)});
      C1 c13 = (C1)c.invoke(new Object[]{new DoubleStandIn(20.0), new C3()});
      C1 c14 = (C1)c.invoke(new Object[]{new FloatStandIn(30.0f), new C2()});
      C1 c15 = (C1)c.invoke(new Object[]{new FloatStandIn(40.0f), new C4()});
    }
    catch(Throwable e)
    {
      System.out.println("Exception: " + e.getMessage());
    }
    try
    {
      C1 c11 = (C1)c.invoke(new Object[]{new IntStandIn(1), new C1()});
    }
    catch(Throwable e)
    {
      System.out.println("Exception: " + e.getMessage());
    }
    System.out.println(c.cache().toString());
  }
  final Class _class;
  final Accessibility _access;
  // The nth element of this collection contains an object with all of the
  // constructors that have n parameters.
  final AllFunctors _constructors;
  final CachedFunctors _cache;
}
