//////////////////////////////////////////////////////////////////////
//
// File Name     : ConstructorFunctor.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/18/1999 20:13:45
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.lang.reflect.*;

class ConstructorFunctor extends Functor
{
  public ConstructorFunctor(Constructor c_)
  {
    _constructor = c_;
  }
  public String getName()
  {
    return(_constructor.getName());
  }
  public String typeName()
  {
    return("constructor");
  }
  public String typeNameCapitalized()
  {
    return("Constructor");
  }
  public String toString()
  {
    return(_constructor.toString());
  }
  public Class[] getParameterTypes()
  {
    return(_constructor.getParameterTypes());
  }
  public Class getDeclaringClass()
  {
    return(_constructor.getDeclaringClass());
  }
  public boolean equals(Object o)
  {
    return(_constructor.equals(o));
  }
  public Method getMethod()
  {
    return(null);
  }
  public Constructor getConstructor()
  {
    return(_constructor);
  }
  public static String toString(Constructor[] ctors_)
  {
    if (ctors_ == null) return(null);
    String result = new String();
    int counter = 0;
    for(int ccounter = 0; ccounter != ctors_.length; ccounter++, counter++)
    {
      if (counter != 0)
	result += '\n';
      result += ctors_[ccounter].toString();
    }
    return(result);
  }
  Constructor _constructor;
}
