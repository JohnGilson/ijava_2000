//////////////////////////////////////////////////////////////////////
//
// File Name     : FunctorInvocationConversion.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/18/1999 16:34:38
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.lang.reflect.*;

import java.util.*;

public class FunctorInvocationConversion
{
  // Determine if the first functor is more specific than the second functor
  // (see Java Language Specification Section 15.11.2.2 Choose the Most
  // Specific Method).
  public static boolean moreSpecific(Functor f1_, Functor f2_)
  {
    if (sameName(f1_, f2_) && sameNumberOfParameters(f1_, f2_))
    {
      Class c1 = f1_.getDeclaringClass();
      Class c2 = f2_.getDeclaringClass();
      if (canConvert(c1, c2))
      {
	Class[] f1ParameterTypes = f1_.getParameterTypes();
	Class[] f2ParameterTypes = f2_.getParameterTypes();
	for(int counter = 0; counter < f1ParameterTypes.length; counter++)
	{
	  Class f1ParameterNType = f1ParameterTypes[counter];
	  Class f2ParameterNType = f2ParameterTypes[counter];
	  if (!canConvert(f1ParameterNType, f2ParameterNType))
	    return(false);
	}
	return(true);
      }
    }
    return(false);
  }
  public static boolean moreSpecific(Method m1_, Method m2_)
  {
    return(moreSpecific(new MethodFunctor(m1_), new MethodFunctor(m2_)));
  }
  public static boolean moreSpecific(Constructor c1_, Constructor c2_)
  {
    return(moreSpecific(new ConstructorFunctor(c1_),
			new ConstructorFunctor(c2_)));
  }
  public static boolean moreSpecific(Class[] types1_, Class[] types2_)
  {
    return(canConvert(types1_, types2_));
  }
  public static boolean moreSpecific(List types1_, List types2_)
  {
    return(canConvert(types1_, types2_));
  }
  public static boolean equals(Functor f1_, Functor f2_)
  {
    return(f1_.equals(f2_));
  }
  public static boolean equals(Class[] types1_, Class[] types2_)
  {
    if (types1_ == null || types2_ == null || types1_.length != types2_.length)
      return(false);
    return(canIdentityConvert(types1_, types2_));
  }
  public static boolean equals(List types1_, List types2_)
  {
    if (types1_ == null || types2_ == null) return(false);
    return(equals(((Class[])types1_.toArray()),
		  ((Class[])types2_.toArray())));
  }
  public static boolean sameName(Functor f1_, Functor f2_)
  {
    return(f1_.getName().equals(f2_.getName()));
  }
  public static boolean sameNumberOfParameters(Functor f1_, Functor f2_)
  {
    Class[] f1ParameterTypes = f1_.getParameterTypes();
    Class[] f2ParameterTypes = f2_.getParameterTypes();
    return(f1ParameterTypes.length == f2ParameterTypes.length);
  }
  // Can a method invocation conversion convert the first argument type to
  // the second argument type?
  public static boolean canConvert(Class p1Class_, Class p2Class_)
  {
    return(canIdentityConvert(p1Class_, p2Class_) ||
	   canWidenConvert(p1Class_, p2Class_));
  }
  public static boolean canConvertWithNoPrimitiveWidening(Class p1Class_,
							  Class p2Class_)
  {
    return(canIdentityConvert(p1Class_, p2Class_) ||
	   canWidenConvertNonPrimitives(p1Class_, p2Class_));
  }
  public static boolean canConvert(Class[] types1_, Class[] types2_)
  {
    if (types1_ == null || types2_ == null || types1_.length != types2_.length)
      return(false);
    for(int i = 0; i < types1_.length; i++)
      if (!canConvert(types1_[i], types2_[i]))
	return(false);
    return(true);
  }
  public static boolean canConvert(List types1_, List types2_)
  {
    if (types1_ == null || types2_ == null) return(false);
    return(canConvert((Class[])types1_.toArray(),
		      (Class[])types2_.toArray()));
  }
  public static boolean canIdentityConvert(Class p1Class_, Class p2Class_)
  {
    return(p1Class_ == p2Class_);
  }
  public static boolean canIdentityConvert(Class[] types1_, Class[] types2_)
  {
    if (types1_ == null || types2_ == null || types1_.length != types2_.length)
      return(false);
    for(int i = 0; i < types1_.length; i++)
      if (!canIdentityConvert(types1_[i], types2_[i])) return(false);
    return(true);
  }
  public static boolean canIdentityConvert(List types1_, List types2_)
  {
    if (types1_ == null || types2_ == null) return(false);
    return(canIdentityConvert(((Class[])types1_.toArray()),
			      ((Class[])types2_.toArray())));
  }
  public static boolean canWidenConvert(Class p1Class_, Class p2Class_)
  {
    if (p1Class_ == null || p2Class_ == null || p1Class_ == p2Class_)
      return(false);
    // Special case. Note: Class.isPrimitive() returns true for void.class.
    if (p2Class_ == void.class)
      return(true);
    boolean p1ClassIsPrimitive = p1Class_.isPrimitive();
    boolean p2ClassIsPrimitive = p2Class_.isPrimitive();
    if (!p1ClassIsPrimitive && !p2ClassIsPrimitive)
      return(canWidenConvertNonPrimitives(p1Class_, p2Class_));
    if (p1ClassIsPrimitive && p2ClassIsPrimitive)
      return(Primitive.canWidenConvert(p1Class_, p2Class_));
    return(false);
  }
  public static boolean canWidenConvertNonPrimitives(Class p1Class_,
						     Class p2Class_)
  {
    if (p1Class_ == null || p2Class_ == null || p1Class_ == p2Class_ ||
	p2Class_ == Null.class)
      return(false);
    if (p2Class_ == void.class) // Special case
      return(true);
    if (p1Class_.isPrimitive() || p2Class_.isPrimitive())
      return(false);
    if (p1Class_ == Null.class)
      return(true);
    // Artificial superclass of all array types.
    if (p1Class_.isArray() && p2Class_ == Utility.AbstractArray.class)
      return(true);
    return(p2Class_.isAssignableFrom(p1Class_));
  }
  public static boolean canWidenConvert(Class[] types1_, Class[] types2_)
  {
    if (types1_ == null || types2_ == null || types1_.length != types2_.length)
      return(false);
    for(int i = 0; i < types1_.length; i++)
      if (!canWidenConvert(types1_[i], types2_[i])) return(false);
    return(true);
  }
  public static boolean canWidenConvert(List types1_, List types2_)
  {
    if (types1_ == null || types2_ == null) return(false);
    return(canWidenConvert(((Class[])types1_.toArray()),
			   ((Class[])types2_.toArray())));
  }
  // Used for test purposes.
  private static class C1
  {
    public void m(float f)
    {
      System.out.println("void C1.m(float)");
    }
    public void m(C1 object)
    {
      System.out.println("void C1.m(C1)");
    }
  }
  // Used for test purposes.
  private static class C2 extends C1
  {
    public void m(int i)
    {
      System.out.println("void C2.m(int)");
    }
    public void m(double d)
    {
      System.out.println("void C2.m(double)");
    }
    public void m(C2 object)
    {
      System.out.println("void C2.m(C2)");
    }
  }
  public static void main(String[] args)
  {
    try
    {
      Method m1 = C2.class.getMethod("m", new Class[]{int.class});
      Functor f1 = new MethodFunctor(m1);
      Method m2 = C2.class.getMethod("m", new Class[]{float.class});
      Functor f2 = new MethodFunctor(m2);
      Method m3 = C2.class.getMethod("m", new Class[]{double.class});
      Functor f3 = new MethodFunctor(m3);
      Method m4 = C2.class.getMethod("m", new Class[]{C1.class});
      Functor f4 = new MethodFunctor(m4);
      Method m5 = C2.class.getMethod("m", new Class[]{C2.class});
      Functor f5 = new MethodFunctor(m5);
      System.out.println("Is void C2.m(int) more specific than " +
			 "void C1.m(float)? " +
			 moreSpecific(f1, f2));
      System.out.println("Is void C1.m(float) more specific than " +
			 "void C2.m(int)? " +
			 moreSpecific(f2, f1));
      System.out.println("Is void C2.m(double) more specific than " +
			 "void C1.m(float)? " +
			 moreSpecific(f3, f2));
      System.out.println("Is void C1.m(float) more specific than " +
			 "void C2.m(double)? " +
			 moreSpecific(f2, f3));
      System.out.println("Is void C2.m(int) more specific than " +
			 "void C2.m(double)? " +
			 moreSpecific(f1, f3));
      System.out.println("Is void C2.m(double) more specific than " +
			 "void C2.m(int)? " +
			 moreSpecific(f3, f1));
      System.out.println("Is void C1.m(C1) more specific than " +
			 "void C2.m(C2)? " +
			 moreSpecific(f4, f5));
      System.out.println("Is void C2.m(C2) more specific than " +
			 "void C1.m(C1)? " +
			 moreSpecific(f5, f4));
      System.out.println("Is void C2.m(C2) more specific than " +
			 "void C1.m(float)? " +
			 moreSpecific(f5, f2));
      System.out.println("Is void C1.m(float) more specific than " +
			 "void C2.m(C2)? " +
			 moreSpecific(f2, f5));
    }
    catch(Throwable t)
    {
      System.out.println(t.getMessage());
    }
  }
}
