//////////////////////////////////////////////////////////////////////
//
// File Name     : Null.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 1/ 7/2000 12:43:20
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

// The Class object associated with this type is used by method lookup
// when the corresponding argument is null.
public class Null
{}
