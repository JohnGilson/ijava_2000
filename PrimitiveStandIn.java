//////////////////////////////////////////////////////////////////////
//
// File Name     : PrimitiveStandIn.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/19/1999 14:48:15
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

// The following are the primitive types requiring stand-in classes:
//   boolean, char, byte, short, int, long, float, and double.
public abstract class PrimitiveStandIn
{
  public abstract Class primitiveClass();
  public abstract Object asObject();
  public static PrimitiveStandIn toStandIn(boolean b_)
  {
    return(new BooleanStandIn(b_));
  }
  public static PrimitiveStandIn toStandIn(byte b_)
  {
    return(new ByteStandIn(b_));
  }
  public static PrimitiveStandIn toStandIn(char c_)
  {
    return(new CharStandIn(c_));
  }
  public static PrimitiveStandIn toStandIn(short s_)
  {
    return(new ShortStandIn(s_));
  }
  public static PrimitiveStandIn toStandIn(int i_)
  {
    return(new IntStandIn(i_));
  }
  public static PrimitiveStandIn toStandIn(long l_)
  {
    return(new LongStandIn(l_));
  }
  public static PrimitiveStandIn toStandIn(float f_)
  {
    return(new FloatStandIn(f_));
  }
  public static PrimitiveStandIn toStandIn(double d_)
  {
    return(new DoubleStandIn(d_));
  }
}
