#ifndef iJava_ToValue_INTERFACE
#define iJava_ToValue_INTERFACE

//////////////////////////////////////////////////////////////////////
//
// File Name     : ToValue.H
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 12/21/1999 12:28:03
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

// From native value to Java value.

#include <functional> // std::unary_function<>

namespace iJava
{

  class Value;

  template<class T>
    class ToValue : public std::unary_function<T, Value>
    {
      typedef std::unary_function<T, Value> BaseType;
      public:
      typedef BaseType::result_type result_type;
    };
  /*
  template<class T>
    inline typename ToValue<T>::result_type toValue(const T & nativeValue_)
    {
      return(ToValue<T>::convert(nativeValue_));
    }
  */
} // namespace

#endif
