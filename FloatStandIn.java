//////////////////////////////////////////////////////////////////////
//
// File Name     : FloatStandIn.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/19/1999 15:27:02
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

public class FloatStandIn extends PrimitiveStandIn
{
  public FloatStandIn(float f_)
  {
    _float = new Float(f_);
  }
  public Object asObject()
  {
    return(_float);
  }
  public Class primitiveClass()
  {
    return(float.class);
  }
  Float _float;
}
