//////////////////////////////////////////////////////////////////////
//
// File Name     : AllFunctors.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/18/1999 20:54:34
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.*;
import java.lang.reflect.*;

class AllFunctors
{
  AllFunctors()
  {
    _functors = new Vector();
  }
  AllFunctors(AllFunctors functors_)
  {
    this();
    Iterator i = functors_.functors().iterator();
    while(i.hasNext())
    {
      AllNParameterOrderedFunctors of_ =
	(AllNParameterOrderedFunctors)i.next();
      if (of_ == null) addList(null);
      else addList(new AllNParameterOrderedFunctors(of_));
    }
  }
  AllFunctors(Functor functor_)
  {
    this();
    add(functor_);
  }
  AllFunctors(Method m_)
  {
    this(new MethodFunctor(m_));
  }
  AllFunctors(Constructor c_)
  {
    this(new ConstructorFunctor(c_));
  }
  AllFunctors(Functor[] functors_)
  {
    this();
    add(functors_);
  }
  AllFunctors(Method[] methods_)
  {
    this();
    add(methods_);
  }
  AllFunctors(Constructor[] constructors_)
  {
    this();
    add(constructors_);
  }
  AllFunctors(List functors_)
  {
    this();
    add(functors_);
  }
  void add(Functor f_)
  {
    int numberOfParameters = f_.getParameterTypes().length;
    if (anyNParameterFunctors(numberOfParameters))
    {
      AllNParameterOrderedFunctors of = get(numberOfParameters);
      of.add(f_);
    }
    else
      set(numberOfParameters, new AllNParameterOrderedFunctors(f_));
  }
  void add(Method m_)
  {
    add(new MethodFunctor(m_));
  }
  void add(Constructor c_)
  {
    add(new ConstructorFunctor(c_));
  }
  void add(Functor[] functors_)
  {
    if (functors_ == null) return;
    for(int i = 0; i < functors_.length; i++)
      add(functors_[i]);
  }
  void add(Method[] methods_)
  {
    if (methods_ == null) return;
    for(int i = 0; i < methods_.length; i++)
      add(methods_[i]);
  }
  void add(Constructor[] constructors_)
  {
    if (constructors_ == null) return;
    for(int i = 0; i < constructors_.length; i++)
      add(constructors_[i]);
  }
  void add(List functors_)
  {
    if (functors_ == null || functors_.isEmpty()) return;
    Object f = functors_.get(0);
    if (f instanceof Functor)
      add((Functor[])functors_.toArray());
    else if (f instanceof Method)
      add((Method[])functors_.toArray());
    else if (f instanceof Constructor)
      add((Constructor[])functors_.toArray());
  }
  List get(Class[] argumentTypes_)
  {
    if (argumentTypes_ == null) return(null);
    int numberOfParameters = argumentTypes_.length;
    if (anyNParameterFunctors(numberOfParameters))
      return(get(numberOfParameters).get(argumentTypes_));
    else return(null);
  }
  List get(Object[] arguments_)
  {
    if (arguments_ == null) return(null);
    int numberOfParameters = arguments_.length;
    if (anyNParameterFunctors(numberOfParameters))
      return(get(numberOfParameters).get(arguments_));
    else return(null);
  }
  List getFromArgClasses(List argumentTypes_)
  {
    if (argumentTypes_ == null) return(null);
    int numberOfParameters = argumentTypes_.size();
    if (anyNParameterFunctors(numberOfParameters))
      return(get(numberOfParameters).getFromArgClasses(argumentTypes_));
    else return(null);
  }
  List getFromArgs(List arguments_)
  {
    if (arguments_ == null) return(null);
    int numberOfParameters = arguments_.size();
    if (anyNParameterFunctors(numberOfParameters))
      return(get(numberOfParameters).getFromArgs(arguments_));
    else return(null);
  }
  List getSuper(Class objectType_, Class[] argumentTypes_)
  {
    if (argumentTypes_ == null) return(null);
    int numberOfParameters = argumentTypes_.length;
    if (anyNParameterFunctors(numberOfParameters))
      return(get(numberOfParameters).getSuper(objectType_, argumentTypes_));
    else return(null);
  }
  List getSuper(Class objectType_, Object[] arguments_)
  {
    if (arguments_ == null) return(null);
    int numberOfParameters = arguments_.length;
    if (anyNParameterFunctors(numberOfParameters))
      return(get(numberOfParameters).getSuper(objectType_, arguments_));
    else return(null);
  }
  List getSuperFromArgClasses(Class objectType_, List argumentTypes_)
  {
    if (argumentTypes_ == null) return(null);
    int numberOfParameters = argumentTypes_.size();
    if (anyNParameterFunctors(numberOfParameters))
      return(get(numberOfParameters).getSuperFromArgClasses(objectType_,
							    argumentTypes_));
    else return(null);
  }
  List getSuperFromArgs(Class objectType_, List arguments_)
  {
    if (arguments_ == null) return(null);
    int numberOfParameters = arguments_.size();
    if (anyNParameterFunctors(numberOfParameters))
      return(get(numberOfParameters).getSuperFromArgs(objectType_,
						      arguments_));
    else return(null);
  }
  AllNParameterOrderedFunctors get(int i_)
  {
    return((AllNParameterOrderedFunctors)functors().get(i_));
  }
  AllNParameterOrderedFunctors getFirstDefined()
  {
    for(int i = 0; i < size(); i++)
      if (anyNParameterFunctors(i)) return(get(i));
    return(null);
  }
  boolean anyNParameterFunctors(int i_)
  {
    return(!(i_ < 0) && !(i_ >= size()) && get(i_) != null);
  }
  void set(int i_, AllNParameterOrderedFunctors f_)
  {
    for(int i = size(); i <= i_; i++)
      functors().add(null);
    functors().set(i_, f_);
  }
  void addList(AllNParameterOrderedFunctors f_)
  {
    functors().add(f_);
  }
  int numberOfFunctors()
  {
    int total = 0;
    for(int i = 0; i < size(); i++)
      if (anyNParameterFunctors(i))
	total += get(i).numberOfFunctors();
    return(total);
  }
  int size()
  {
    return(functors().size());
  }
  boolean isEmpty()
  {
    return(functors().isEmpty());
  }
  public String toString()
  {
    return(toString(0));
  }
  public String toString(int indent_)
  {
    final int ownIndent = 2 + indent_;
    String indentation = "";
    for(int i = 0; i < indent_; i++)
      indentation += ' ';
    if (!functors().isEmpty())
    {
      Functor f = getFirstDefined().get(0).getMostSpecific();
      String name = f.getName();
      String s =
	indentation + f.typeNameCapitalized() + 's' + " named " + name + '\n';
      s += (indentation + "[\n");
      for(int i = 0; i < size(); i++)
      {
	AllNParameterOrderedFunctors of = get(i);
	if (of != null)
	  s += (of.toString(ownIndent) + '\n');
      }
      s += (indentation + ']');
      return(s);
    }
    else return("");
  }
  List functors()
  {
    return(_functors);
  }
  final List _functors;
}
