//////////////////////////////////////////////////////////////////////
//
// File Name     : FieldResolversByClass.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 1/21/2000 11:48:19
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.*;
import java.lang.reflect.*;

public class FieldResolversByClass
{
  public FieldResolversByClass()
  {
    this(new Accessibility());
  }
  public FieldResolversByClass(Accessibility access_)
  {
    _classToFieldResolvers = new Hashtable();
    _access = access_;
  }
  ByName get(Class class_)
  {
    return((ByName)_classToFieldResolvers.get(class_));
  }
  public FieldResolver get(Class class_, String fieldName_)
  {
    ByName allFieldsClass = get(class_);
    if (allFieldsClass == null) return(null);
    else return(allFieldsClass.get(fieldName_));
  }
  public FieldResolver get(Object object_, String fieldName_)
  {
    return(get(object_.getClass(), fieldName_));
  }
  public FieldResolver get(String className_, String fieldName_)
  {
    try
    {
      return(get(Class.forName(className_), fieldName_));
    }
    catch(Exception e) {return(null);}
  }
  public FieldResolver put(Object object_, String fieldName_)
  {
    return(put(object_ == null ? null : object_.getClass(), fieldName_));
  }
  public FieldResolver put(Class class_, String fieldName_)
  {
    if (class_ == null || fieldName_ == null) return(null);
    ByName classEntry = get(class_);
    boolean classEntryNew = false;
    if (classEntry == null)
    {
      classEntry = new ByName();
      _classToFieldResolvers.put(class_, classEntry);
      classEntryNew = true;
    }
    FieldResolver fr = classEntry.get(fieldName_);
    if (fr == null)
    {
      Field f = FieldResolver.getClassDefinedField(class_, fieldName_,
						   _access);
      if (f == null)
      {
	Class superClass = class_.getSuperclass();
	Class[] interfaces = class_.getInterfaces();
	int immediateSuperTypes = 0;
	if (superClass != null) immediateSuperTypes++;
	immediateSuperTypes += interfaces.length;
	FieldResolver[] frs = new FieldResolver[immediateSuperTypes];
	int countFRs = 0;
	if (superClass != null)
	  frs[countFRs++] = put(superClass, fieldName_);
	for(int counter = 0; counter < interfaces.length; counter++)
	  frs[countFRs++] = put(interfaces[counter], fieldName_);
	FieldResolver newFR =
	  new FieldResolver(class_, fieldName_, _access, frs, false);
	if (newFR.isEmpty())
	{
	  if (classEntryNew)
	    _classToFieldResolvers.remove(class_);
	  return(null);
	}
	else
	{
	  classEntry.put(fieldName_, newFR);
	  return(newFR);
	}
      }
      else
      {
	FieldResolver newFR =
	  new FieldResolver(class_, fieldName_, _access, f);
	classEntry.put(fieldName_, newFR);
	return(newFR);
      }
    }
    else return(fr);
  }
  public boolean contains(Class class_)
  {
    return(get(class_) != null);
  }
  public boolean contains(Class class_, String fieldName_)
  {
    ByName allFieldsClass = get(class_);
    if (allFieldsClass != null)
      return(allFieldsClass.contains(fieldName_));
    else return(false);
  }
  public String toString()
  {
    return(toString(0));
  }
  public String toString(int indent_)
  {
    final int ownIndent = 2;
    String indentation = "";
    String ownIndentation = "";
    for(int i = 0; i < indent_; i++)
      indentation += ' ';
    for(int i = 0; i < ownIndent; i++)
      ownIndentation += ' ';
    if (!isEmpty())
    {
      Set classes = _classToFieldResolvers.entrySet();
      Iterator iteratorClasses = classes.iterator();
      String s = indentation + "Fields by type\n";
      s += (indentation + "[\n");
      while(iteratorClasses.hasNext())
      {
	Map.Entry classEntry = (Map.Entry)iteratorClasses.next();
	String className = ((Class)classEntry.getKey()).toString();
	s += (indentation + ownIndentation + className + '\n');
	s += (indentation + ownIndentation + "[\n");
	s += ((ByName)classEntry.getValue()).toString(indent_ + 2 * ownIndent);
	s += '\n';
	s += (indentation + ownIndentation + "]\n");
      }
      s += (indentation + ']');
      return(s);
    }
    else return("");
  }
  public boolean isEmpty()
  {
    return(_classToFieldResolvers.isEmpty());
  }
  public int size()
  {
    return(_classToFieldResolvers.size());
  }
  class ByName
  {
    ByName()
    {
      _nameToFieldTable = new Hashtable();
    }
    FieldResolver get(String fieldName_)
    {
      return((FieldResolver)_nameToFieldTable.get(fieldName_));
    }
    boolean contains(String fieldName_)
    {
      return(get(fieldName_) != null);
    }
    void put(String fieldName_, FieldResolver fr_)
    {
      _nameToFieldTable.put(fieldName_, fr_);
    }
    int size()
    {
      return(_nameToFieldTable.size());
    }
    boolean isEmpty()
    {
      return(_nameToFieldTable.isEmpty());
    }
    public String toString()
    {
      return(toString(0));
    }
    public String toString(int indent_)
    {
      if (!isEmpty())
      {
	Set fds = _nameToFieldTable.entrySet();
	String s = "";
	Iterator iterator = fds.iterator();
	while(iterator.hasNext())
	{
	  Map.Entry fdEntry = (Map.Entry)iterator.next();
	  FieldResolver fr = (FieldResolver)fdEntry.getValue();
	  s += fr.toString(indent_);
	  if (iterator.hasNext()) s += '\n';
	}
	return(s);
      }
      else return("");
    }
    // For a given reference type, map each field name to its accessible
    // field (or fields).
    final Map _nameToFieldTable;
  }
  // Testing
  static class C1
  {
    int f1C1 = 1;
    String f2C1 = "hello";
  }
  interface I1
  {
    static int f1I1 = 10;
  }
  interface I2 extends I1
  {
    static double f1I1 = 100.0;
  }
  interface I3 extends I1
  {
  }
  static class C2 extends C1 implements I2, I3
  {
    final float f1C1 = 1000.0f;
  }
  public static void main(String[] args_)
  {
    FieldResolversByClass frs = new FieldResolversByClass();
    frs.put(C2.class, "f1C1");
    frs.put(C2.class, "f2C1");
    frs.put(C2.class, "f1I1");
    System.out.println(frs);
    frs.put(C1.class, "f1C1");
    System.out.println(frs.get(C1.class));
    System.out.println(frs.put(C1.class, "f2C1").fields() ==
		       frs.put(C2.class, "f2C1").fields());
    System.out.println(frs.put(C1.class, "f1C1").fields() ==
		       frs.put(C2.class, "f1C1").fields());
  }
  final Map _classToFieldResolvers;
  final Accessibility _access;
}
