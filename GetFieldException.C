//////////////////////////////////////////////////////////////////////
//
// File Name     : GetFieldException.C
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 3/27/2000 16:41:25
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

#include <iJava/GetFieldException.H>

namespace iJava
{

  const std::string & GetFieldException::messageDefault()
  {
    static std::string s(messagePreface() + "GetFieldException");
    return(s);
  }

} // namespace
