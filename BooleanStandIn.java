//////////////////////////////////////////////////////////////////////
//
// File Name     : BooleanStandIn.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/19/1999 15:21:10
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

public class BooleanStandIn extends PrimitiveStandIn
{
  public BooleanStandIn(boolean b_)
  {
    _boolean = new Boolean(b_);
  }
  public Object asObject()
  {
    return(_boolean);
  }
  public Class primitiveClass()
  {
    return(boolean.class);
  }
  Boolean _boolean;
}
