//////////////////////////////////////////////////////////////////////
//
// File Name     : IOU.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 9/27/1999 16:53:08
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.io.*;

public class IOU
{
  public synchronized Object get()
  {
    if (_result == null)
      waitForPayment();
    return(_result);
  }
  protected synchronized void waitForPayment()
  {
    try
    {
      wait();
    }
    catch(Exception e)
    {}
  }
  public synchronized void set(Object object_)
  {
    _result = object_;
    notify();
  }
  public synchronized void set()
  {
    set(_dummy);
  }
  public synchronized boolean canCollect()
  {
    get();
    return(true);
  }
  public synchronized boolean isEmptyPromise()
  {
    return(_result == null || _result == _dummy);
  }
  // Test
  public static void main(String[] args)
  {
    final IOU iou = new IOU();
    Runnable r = new Runnable()
      {
	public void run()
	  {
	    System.out.println("Get object from IOU...");
	    Object o = iou.get();
	    System.out.println("IOU delivers " + o);
	  }
      };
    new Thread(r).start();
    try
    {
      Thread.sleep(2000);
    }
    catch(Exception e) {}
    System.out.print("Enter an integer: ");
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try
    {
      int i = Integer.parseInt(br.readLine());
      iou.set(new Integer(i));
    }
    catch(Exception e)
    {
      System.out.println(e.getMessage());
    }
  }
  private Object _result = null;
  private static Object _dummy = new Object();
}
