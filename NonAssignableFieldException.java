//////////////////////////////////////////////////////////////////////
//
// File Name     : NonAssignableFieldException.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 1/20/2000 16:02:27
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.lang.reflect.*;

public class NonAssignableFieldException extends Exception
{
  public NonAssignableFieldException()
  {
    super(_defaultMessage);
    _field = null;
  }
  public NonAssignableFieldException(Field field_)
  {
    super(toString(_defaultMessage, field_));
    _field = field_;
  }
  public NonAssignableFieldException(String msg_)
  {
    super(msg_);
    _field = null;
  }
  public NonAssignableFieldException(String msg_, Field field_)
  {
    super(toString(msg_, field_));
    _field = field_;
  }
  public Field field()
  {
    return(_field);
  }
  static String toString(String message_, Field field_)
  {
    if (field_ != null)
    {
      String s = message_ + '\n' + field_.toString();
      return(s);
    }
    else return(message_);
  }
  public String toString()
  {
    return(toString(_defaultMessage, _field));
  }
  final Field _field;
  static final String _defaultMessage =
    "Field is final and therefore can't be assigned to.";
}
