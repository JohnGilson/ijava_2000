//////////////////////////////////////////////////////////////////////
//
// File Name     : IOU.C
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 3/22/2000 15:16:40
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

#include <iJava/IOU.H>
#include <iJava/Class.H>
#include <iJava/String.H>
#include <iJava/Primitive.H>
#include <iJava/ClassOf.H>
#include <iJava/ClassCastException.H>
#include <iJava/InvokeException.H>
#include <iJava/NullPointerException.H>

namespace iJava
{

  IOU::IOU(JNativeType o_) throw(ClassCastException)
    : BaseType(ClassOf<IOU>::get().isTypeOf(o_) ? o_ :
	       (throw ClassCastException("ClassCastException because Object"
					 " not of type iJava.IOU."),
		(JNativeType)NULL))
  {}

  IOU::IOU(const Object & o_) throw(ClassCastException)
    : BaseType(ClassOf<IOU>::get().isTypeOf(o_) ?
	       newReference(o_, this) :
	       (throw ClassCastException("ClassCastException because Object"
					 " not of type iJava.IOU."),
		std::pair<BaseType::JNativeType, ReferenceType>()))
  {}

  Object IOU::get() const throw(InvokeException, NullPointerException)
  {
    return(invoke("get"));
  }

  Boolean IOU::isEmptyPromise()
  const throw(InvokeException, ClassCastException, NullPointerException)
  {
    Boolean result(invoke("isEmptyPromise"));
    return(result);
  }

  void IOU::set() const throw(InvokeException, NullPointerException)
  {
    invoke("set");
  }

  void IOU::set(const Object & o_) const
  throw(InvokeException, NullPointerException)
  {
    invoke("set", o_);
  }

  const IOU & IOU::null()
  {
    static GlobalRef<IOU> n;
    return(n);
  }

  const std::string & IOU::getFullyQualifiedName()
  {
    static std::string name("iJava.IOU");
    return(name);
  }

  const std::string & IOU::getClassDescriptor()
  {
    static std::string name(getFullyQualifiedName());
    static std::string & classDesc = BaseType::getClassDescriptor(name);
    return(classDesc);
  }

  const std::string & IOU::getFieldDescriptor()
  {
    static std::string classDesc(getClassDescriptor());
    static std::string & fieldDesc = BaseType::getFieldDescriptor(classDesc);
    return(fieldDesc);
  }

} // namespace
