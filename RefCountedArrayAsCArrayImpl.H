#ifndef iJava_RefCountedArrayAsCArrayImpl_INTERFACE
#define iJava_RefCountedArrayAsCArrayImpl_INTERFACE

//////////////////////////////////////////////////////////////////////
//
// File Name     : RefCountedArrayAsCArrayImpl.H
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 5/ 3/2000 19:56:07
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

#include <iJava/RefCountedResource.H>
#include <iJava/CArrayTraits.H>
#include <iJava/Array.H>
#include <iJava/ArrayIndexOutOfBoundsException.H>

#include <jni.h>

namespace iJava
{

  template<class T>
    class RefCountedArrayAsCArrayImpl :
    public Utility::RefCountedArrayImpl<typename CArrayTraits<T>::EltType>
    {
      typedef Utility::RefCountedArrayImpl<typename CArrayTraits<T>::EltType>
	BaseType;
      public:
      class ResourceType;
      friend class ResourceType;
      typedef RefCountedArrayAsCArrayImpl<T> SelfType;
      typedef typename CArrayTraits<T>::EltType EltType;
      typedef typename CArrayTraits<T>::ArrayType ArrayType;
      typedef EltType * iterator;
      class ResourceType : public BaseType::ResourceType
      {
	typedef BaseType::ResourceType BResourceType;
	public:
	ResourceType()
	  : pjarray(0), startPos(0), isBufferProvided(false)
	{}
	ResourceType(const Array<T> * pArray_)
	  : BResourceType((pArray_ == 0 ? 0 :
			   CArrayTraits<T>::getCArray(*pArray_)),
			  (pArray_ == 0 ? 0 : pArray_->length())),
	    pjarray(pArray_), startPos(0), isBufferProvided(false)
	{}
	ResourceType(const Array<T> & array_)
	  : BResourceType(CArrayTraits<T>::getCArray(array_), array_.length()),
	    pjarray(&array_), startPos(0), isBufferProvided(false)
	{}
	ResourceType(const Array<T> & array_, jsize startPos_,
		     iterator start_, iterator end_)
	  : BResourceType(start_, end_ - start_), pjarray(&array_),
	    startPos(startPos_), isBufferProvided(true)
	    throw(ArrayIndexOutOfBoundsException)
	{
	  CArrayTraits<T>::getCArray(array_, startPos_, start_, end_);
	}
	ResourceType(const Array<T> & array_,
		     EltType * carray_,
		     unsigned int length_,
		     jsize startPos_, bool isBufferProvided_)
	  : BResourceType(carray_, length_), pjarray(&array_),
	    startPos(startPos_), isBufferProvided(isBufferProvided_)
	{}
	const Array<T> * pjarray;
	jsize startPos;
	bool isBufferProvided;
      };
      RefCountedArrayAsCArrayImpl()
	: _array(0), _startPos(0), _isBufferProvided(false)
      {}
      RefCountedArrayAsCArrayImpl(const ResourceType & r_)
	: BaseType(r_), _array(r_.pjarray), _startPos(r_.startPos),
	  _isBufferProvided(r_.isBufferProvided)
      {}
      void dropReference() throw(ArrayIndexOutOfBoundsException)
      {
	_count.dropReference();
	if (_count.noReferences() && hasResource())
	{
	  if (_isBufferProvided)
	    CArrayTraits<T>::setCArray(*_array, _startPos, _p, _p + _length);
	  else
	    CArrayTraits<T>::setCArray(*_array, _p);
	  _array = 0;
	  _startPos = 0;
	  _isBufferProvided = false;
	  _length = 0;
	  _p = 0;
	}
      }
      ResourceType getResource() const
      {
	if (_array == 0)
	  return(ResourceType());
	typename BaseType::ResourceType br = BaseType::getResource();
	return(ResourceType(*_array, br.array, br.length, _startPos,
			    _isBufferProvided));
      }
      ResourceType releaseResource()
      {
	if (_array == 0)
	  return(ResourceType());
	typename BaseType::ResourceType br = BaseType::releaseResource();
	return(ResourceType(*_array, br.array, br.length, _startPos,
			    _isBufferProvided));
      }
      protected:
      const Array<T> * _array;
      jsize _startPos;
      bool _isBufferProvided;
    };

} // namespace

#endif
