//////////////////////////////////////////////////////////////////////
//
// File Name     : IOUTimed.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 5/ 2/2000 11:17:45
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.io.*;

public class IOUTimed extends IOU
{
  public IOUTimed()
  {}
  public IOUTimed(long msTimeout_)
  {
    _msTimeout = msTimeout_;
  }
  public IOUTimed(long msTimeout_, int nsTimeout_)
  {
    this(msTimeout_);
    _nsTimeout = nsTimeout_;
  }
  protected synchronized void waitForPayment()
  {
    try
    {
      if (_msTimeout != 0)
      {
	if (_nsTimeout != 0)
	  wait(_msTimeout, _nsTimeout);
	else wait(_msTimeout);
      }
      else if (_nsTimeout != 0)
	wait(_msTimeout, _nsTimeout);
      else
	wait();
    }
    catch(Exception e)
    {}
  }
  // Test
  public static void main(String[] args)
  {
    final IOUTimed iou = new IOUTimed(2000);
    Runnable r = new Runnable()
      {
	public void run()
	  {
	    System.out.println("Get object from IOU...");
	    Object o = iou.get();
	    System.out.println("IOU delivers " + o);
	  }
      };
    new Thread(r).start();
    try
    {
      Thread.sleep(4000);
    }
    catch(Exception e) {}
    System.out.print("Enter an integer: ");
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try
    {
      int i = Integer.parseInt(br.readLine());
      iou.set(new Integer(i));
    }
    catch(Exception e)
    {
      System.out.println(e.getMessage());
    }
  }
  private long _msTimeout = 0;
  private int _nsTimeout = 0;
}
