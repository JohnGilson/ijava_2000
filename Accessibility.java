//////////////////////////////////////////////////////////////////////
//
// File Name     : Accessibility.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 10/ 1/1999 10:51:29
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.lang.reflect.*;

// Specify access to members (methods, constructors and fields) of a class.
public class Accessibility
{
  public Accessibility()
  {
    _access = Public | Package;
  }
  public Accessibility(byte access_)
  {
    if (access_ == Public) _access = accessPublic();
    else if (access_ == Package) _access = accessPackage();
    else if (access_ == Private) _access = accessPrivate();
    else _access = accessUnspecified();
  }
  public static byte accessPublic()
  {
    return(Public);
  }
  public static byte accessPackage()
  {
    return(Public | Package);
  }
  public static byte accessPrivate()
  {
    return(Public | Package | Private);
  }
  public static byte accessUnspecified()
  {
    return(Unspecified);
  }
  public byte access()
  {
    return(_access);
  }
  public boolean hasPublic()
  {
    return((_access & Public) == Public);
  }
  public boolean hasPackage()
  {
    return((_access & Package) == Package);
  }
  public boolean hasPrivate()
  {
    return((_access & Private) == Private);
  }
  public boolean hasDefault()
  {
    return(hasPackage());
  }
  public boolean isUnspecified()
  {
    return(_access == Unspecified);
  }
  public String toString()
  {
    if (hasPrivate()) return("public, protected, default, or private access");
    if (hasPackage()) return("public, protected, or default access");
    if (hasPublic()) return("public access");
    if (isUnspecified()) return("unspecified access");
    return("");
  }
  public class Exception extends java.lang.Exception
  {
    public Exception()
    {}
    public Exception(String msg_)
    {
      super(msg_);
    }
    public Exception(int modifiers_)
    {
      super("Reflective object with modifiers " +
	    Modifier.toString(modifiers_) +
	    " inaccessible when " + Accessibility.this.toString() +
	    " is specified.");
    }
  }
  public static void main(String[] args)
  {
    Accessibility a1 = new Accessibility();
    System.out.println("Accessibility is " + a1.access());
    System.out.println("Has public accessibility? " + a1.hasPublic());
    System.out.println("Has package accessibility? " + a1.hasPackage());
    Accessibility a2 = new Accessibility(Public);
    System.out.println("Accessibility is " + a2.access());
    System.out.println("Has public accessibility? " + a2.hasPublic());
    System.out.println("Has package accessibility? " + a2.hasPackage());
    Accessibility a3 = new Accessibility(Private);
    System.out.println("Accessibility is " + a3.access());
    System.out.println("Has public accessibility? " + a3.hasPublic());
    System.out.println("Has package accessibility? " + a3.hasPackage());
    System.out.println("Has private accessibility? " + a3.hasPrivate());
    try
    {
      throw a2.new Exception(Modifier.PRIVATE);
    }
    catch(Exception e_)
    {
      System.out.println(e_);
    }
  }
  final byte _access;
  // unspecified accessibility
  public static final byte Unspecified = 0x00;
  // public members are accessible
  public static final byte Public = 0x01;
  // all but private members are accessible
  public static final byte Package = 0x02;
  // all members are accessible
  public static final byte Private = 0x04;
  // default accessibility
  public static final byte Default = Package;
}
