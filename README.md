        iJava: A library to support integrating Java with C++

JNI (Java Native Interface) provides a low-level, verbose and
error-prone API to integrate Java with C and C++ code running within
the same process. The iJava API is an attempt to make this
higher-level, more succinct and less error-prone. One of the major
advantages of iJava is that Java method resolution (the determination
of what Java method to invoke at each method call site) is done
completely by iJava, unlike JNI where the programmer must specify
exactly what Java method to invoke and keep the specification current
in the face of changes to the Java code.

Implemented in Standard C++ (C++98) using the Solaris C++ compiler and
Java (J2SE 1.2).

See ComparisonExample.txt for a comparison of an example solution in
Java, in C using JNI and in C++ using iJava.

See testiJava.C for iJava examples. This has a number of examples
which exercise the iJava functionality and is the closest thing to
proper documentation!

Designed and successfully implemented by John Gilson from September
1999 to May 2000. No further work after this time.
