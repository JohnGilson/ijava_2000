//////////////////////////////////////////////////////////////////////
//
// File Name     : ShortStandIn.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/19/1999 15:24:07
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

public class ShortStandIn extends PrimitiveStandIn
{
  public ShortStandIn(short s_)
  {
    _short = new Short(s_);
  }
  public Object asObject()
  {
    return(_short);
  }
  public Class primitiveClass()
  {
    return(short.class);
  }
  Short _short;
}
