package iJava;

import java.lang.reflect.*;
import java.util.*;

public class test
{
  public static void main(String[] args)
  {
    C2 obj1 = new C2();
    obj1.m(10);
    obj1.m(10.0);
    obj1.n(10.0);
    Method[] methods = C2.class.getMethods();
    System.out.println("***Print all public methods for C2.***");
    for(int counter = 0; counter < methods.length; counter++)
      System.out.println(methods[counter]);
    methods = C2.class.getDeclaredMethods();
    System.out.println("***Print all methods for C2.***");
    for(int counter = 0; counter < methods.length; counter++)
      System.out.println(methods[counter]);
    System.out.print("The class name for int is ");
    System.out.println(int.class.getName());
    //int [][] ia = new int[10][];
    int[] ia2 = new int[0];
    System.out.println("The number of elements in ia2 is " + ia2.length);
    System.out.println(C2.class.getName());
    Object[] oa = new Object[0];
    List l = Arrays.asList(oa);
    System.out.println("The null array as list is " + l);
    List l1 = new Vector();
    l1.add(new Integer(1));
    l1.add(new Integer(2));
    l1.add(new Integer(3));
    ListIterator i1 = l1.listIterator();
    i1.next();
    i1.add(new Integer(4));
    System.out.println(l1);
    System.out.println(test.class.getClass().getName());
    Object o1 = null;
    String s1 = null;
    System.out.println(o1 == s1);
    int[] a1 = new int[]{1, 2, 3};
    int[] a2 = new int[]{1, 2, 3};
    int[] a3 = a1;
    Object o2 = new Object();
    Object[] oa1 = new Object[]{o2};
    Object[] oa2 = new Object[]{o2};
    System.out.println(a1.equals(a2));
    System.out.println(a1.equals(a3));
    System.out.println(oa1.equals(oa2));
    CF2 objCF2 = new CF2();
    System.out.println(objCF2.i);
    System.out.println(((CF1)objCF2).i);
    System.out.println("Class of void " + void.class);
    try
    {
      System.out.println(C1.class.getDeclaredMethod("mp", new Class[0]));
    }
    catch(Exception e_)
    {
      System.out.println(e_.getMessage());
    }
    int[] ia3 = new int[]{2, 4, 6, 8, 10};
    String ia3toString  = ia3.toString();
    System.out.println(ia3toString);
    System.out.print('[');
    for(int counter = 0; counter < ia3.length;)
    {
      System.out.print(ia3[counter++]);
      if (counter != ia3.length)
	System.out.print(' ');
    }
    System.out.print(']');
    Vector v1 = new Vector();
    v1.add(new String("ab,c"));
    v1.add(new String("de,f"));
    v1.add(new String("gh,i"));
    System.out.println(v1);
    Object[] oa3 = new int[2][10];
    System.out.println(Object[].class.getName());
    System.out.println(Object.class.getName());
    System.out.println(int.class.getName());
    System.out.println(void.class.getName());
    /*
      Catch must be a block.
      This is an error.
      try
      {
      System.out.println("throw");
      }
      catch(Exception e_)
      System.out.println("catch");
    */
    Class cString = String.class;
    Package p = cString.getPackage();
    System.out.println(p.getName());
    System.out.println(p.getName().equals("java.lang"));
    System.out.println(new java.lang.reflect.InvocationTargetException
		       (new ArrayIndexOutOfBoundsException("Out of bounds.")));
    Package[] packages = Package.getPackages();
    System.out.println("All Packages:\n" + Utility.toString(packages, ",\n"));
  }
}

class C1
{
  public void m(double f)
  {
    System.out.println("void C1.m(double)");
  }
  public void m(int i)
  {
    System.out.println("void C1.m(int)");
  }
  private void mp()
  {}
}

class C2 extends C1
{
  public C2()
  {}
  public void m(int i)
  {
    System.out.println("void C2.m(int)");
  }
  public void n(double f)
  {
    super.m(f);
  }
}

class CF1
{
  public int i = 1;
}

class CF2 extends CF1
{
  public int i = 2;
}
