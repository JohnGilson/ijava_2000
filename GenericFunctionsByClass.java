//////////////////////////////////////////////////////////////////////
//
// File Name     : GenericFunctionsByClass.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 10/ 4/1999 10:14:08
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.*;

public class GenericFunctionsByClass
{
  public GenericFunctionsByClass()
  {
    _classToGenericFunctions = new Hashtable();
    _access = new Accessibility();
  }
  public GenericFunctionsByClass(Accessibility access_)
  {
    _classToGenericFunctions = new Hashtable();
    _access = access_;
  }
  ByName get(Class class_)
  {
    return((ByName)_classToGenericFunctions.get(class_));
  }
  public GenericFunction get(Class class_, String methodName_)
  {
    ByName allMethodsClass = get(class_);
    if (allMethodsClass == null) return(null);
    else return(allMethodsClass.get(methodName_));
  }
  public GenericFunction get(Object object_, String methodName_)
  {
    return(get(object_.getClass(), methodName_));
  }
  public GenericFunction get(String className_, String methodName_)
  {
    try
    {
      return(get(Class.forName(className_), methodName_));
    }
    catch(Exception e) {return(null);}
  }
  public boolean contains(Class class_)
  {
    return(get(class_) != null);
  }
  public boolean contains(Class class_, String methodName_)
  {
    ByName allMethodsClass = get(class_);
    if (allMethodsClass != null)
      return(allMethodsClass.contains(methodName_));
    else return(false);
  }
  // Puts generic function if not there.
  public GenericFunction put(Class class_, String methodName_)
  {
    if (class_ == null || methodName_ == null) return(null);
    ByName classEntry = get(class_);
    boolean classEntryNew = false;
    if (classEntry == null)
    {
      classEntry = new ByName();
      _classToGenericFunctions.put(class_, classEntry);
      classEntryNew = true;
    }
    GenericFunction gf = classEntry.get(methodName_);
    if (gf == null)
    {
      Class superClass = class_.getSuperclass();
      if (superClass == null)
      {
	GenericFunction newGF =
	  new GenericFunction(class_, methodName_, _access);
	if (newGF.isEmpty())
	{
	  if (classEntryNew) _classToGenericFunctions.remove(class_);
	  return(null);
	}
	else
	{
	  classEntry.put(methodName_, newGF);
	  return(newGF);
	}
      }
      else
      {
	GenericFunction gfSuper = put(superClass, methodName_);
	if (gfSuper == null)
	{
	  GenericFunction newGF =
	    new GenericFunction(class_, methodName_, _access);
	  if (newGF.isEmpty())
	  {
	    if (classEntryNew) _classToGenericFunctions.remove(class_);
	    return(null);
	  }
	  else
	  {
	    classEntry.put(methodName_, newGF);
	    return(newGF);
	  }
	}
	else
	{
	  GenericFunction newGF = new GenericFunction(class_, gfSuper);
	  classEntry.put(methodName_, newGF);
	  return(newGF);
	}
      }
    }
    else return(gf);
  }
  public String toString()
  {
    return(toString(0));
  }
  public String toString(int indent_)
  {
    final int ownIndent = 2;
    String indentation = "";
    String ownIndentation = "";
    for(int i = 0; i < indent_; i++)
      indentation += ' ';
    for(int i = 0; i < ownIndent; i++)
      ownIndentation += ' ';
    if (!isEmpty())
    {
      Set classes = _classToGenericFunctions.entrySet();
      Iterator iteratorClasses = classes.iterator();
      String s = indentation + "Generic Functions\n";
      s += (indentation + "[\n");
      while(iteratorClasses.hasNext())
      {
	Map.Entry classEntry = (Map.Entry)iteratorClasses.next();
	String className = ((Class)classEntry.getKey()).getName();
	s += (indentation + ownIndentation + "Class " + className + '\n');
	s += (indentation + ownIndentation + "[\n");
	s += ((ByName)classEntry.getValue()).toString(indent_ + 2 * ownIndent);
	s += '\n';
	s += (indentation + ownIndentation + "]\n");
      }
      s += (indentation + ']');
      return(s);
    }
    else return("");
  }
  public boolean isEmpty()
  {
    return(_classToGenericFunctions.isEmpty());
  }
  public int size()
  {
    return(_classToGenericFunctions.size());
  }
  class ByName
  {
    ByName()
    {
      _nameToGenericFunction = new Hashtable();
    }
    GenericFunction get(String methodName_)
    {
      return((GenericFunction)_nameToGenericFunction.get(methodName_));
    }
    boolean contains(String methodName_)
    {
      return(get(methodName_) != null);
    }
    void put(String methodName_, GenericFunction gf_)
    {
      _nameToGenericFunction.put(methodName_, gf_);
    }
    int size()
    {
      return(_nameToGenericFunction.size());
    }
    boolean isEmpty()
    {
      return(_nameToGenericFunction.isEmpty());
    }
    public String toString()
    {
      return(toString(0));
    }
    public String toString(int indent_)
    {
      if (!isEmpty())
      {
	Set gfs = _nameToGenericFunction.entrySet();
	String s = "";
	Iterator iterator = gfs.iterator();
	while(iterator.hasNext())
	{
	  Map.Entry gfEntry = (Map.Entry)iterator.next();
	  GenericFunction gf = (GenericFunction)gfEntry.getValue();
	  s += gf.toString(indent_);
	  if (iterator.hasNext()) s += '\n';
	}
	return(s);
      }
      else return("");
    }
    // For a given class, map each method name to all its methods.
    final Map _nameToGenericFunction;
  }
  // Testing
  static class C1
  {
    void m1(double d) {}
    void m2(double d) {}
  }
  static class C2 extends C1
  {
    void m1(int i) {}
    void m2(float f) {}
    void m3(boolean b) {}
  }
  static class C3 extends C2
  {
    void m1(int i) {}
    void m3(boolean b) {}
  }
  static void main(String[] args)
  {
    GenericFunctionsByClass allGF = new GenericFunctionsByClass();
    allGF.put(C3.class, "m1");
    allGF.put(C3.class, "m2");
    allGF.put(C3.class, "m3");
    System.out.println(allGF.toString());
    GenericFunction gfC3m2 = allGF.get(C3.class, "m2");
    GenericFunction gfC2m2 = allGF.get(C2.class, "m2");
    System.out.println("Is the C2.m2 methods object the same as the " +
		       "C3.m2 methods object? " +
		       (gfC3m2.methods() == gfC2m2.methods()));
    GenericFunction gfC3m3 = allGF.get(C3.class, "m3");
    GenericFunction gfC2m3 = allGF.get(C2.class, "m3");
    System.out.println("Is the C2.m3 methods object the same as the " +
		       "C3.m3 methods object? " +
		       (gfC3m3.methods() == gfC2m3.methods()));
  }
  // Each class maps to all its methods.
  final Map _classToGenericFunctions;
  final Accessibility _access;
}
