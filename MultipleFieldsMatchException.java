//////////////////////////////////////////////////////////////////////
//
// File Name     : MultipleFieldsMatchException.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 1/19/2000 15:30:29
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.*;
import java.lang.reflect.*;

public class MultipleFieldsMatchException extends Exception
{
  public MultipleFieldsMatchException()
  {
    super(_defaultMessage);
    _fields = null;
  }
  public MultipleFieldsMatchException(Field[] fields_)
  {
    super(toString(_defaultMessage, fields_));
    _fields = fields_;
  }
  public MultipleFieldsMatchException(List fields_)
  {
    super(toString(_defaultMessage, (Field[])fields_.toArray()));
    _fields = (Field[])fields_.toArray();
  }
  public MultipleFieldsMatchException(String msg_)
  {
    super(msg_);
    _fields = null;
  }
  public MultipleFieldsMatchException(String msg_, Field[] fields_)
  {
    super(toString(msg_, fields_));
    _fields = fields_;
  }
  public MultipleFieldsMatchException(String msg_, List fields_)
  {
    super(toString(msg_, (Field[])fields_.toArray()));
    _fields = (Field[])fields_.toArray();
  }
  public Field[] fields()
  {
    return(_fields);
  }
  static String toString(String message_, Field[] fields_)
  {
    if (fields_ != null)
    {
      String s = message_;
      for(int counter = 0; counter < fields_.length; counter++)
      {
	s += '\n';
	s += fields_[counter].toString();
      }
      return(s);
    }
    else return(message_);
  }
  public String toString()
  {
    return(toString(_defaultMessage, _fields));
  }
  final Field[] _fields;
  static final String _defaultMessage = "Multiple fields match access.";
}
