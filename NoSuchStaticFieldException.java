//////////////////////////////////////////////////////////////////////
//
// File Name     : NoSuchStaticFieldException.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 1/19/2000 19:31:45
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.lang.reflect.*;

public class NoSuchStaticFieldException extends Exception
{
  public NoSuchStaticFieldException()
  {
    super(_defaultMessage);
    _field = null;
  }
  public NoSuchStaticFieldException(Field field_)
  {
    super(_defaultMessage);
    _field = field_;
  }
  public NoSuchStaticFieldException(String msg_)
  {
    super(msg_);
    _field = null;
  }
  public NoSuchStaticFieldException(String msg_, Field field_)
  {
    super(msg_);
    _field = field_;
  }
  public Field field()
  {
    return(_field);
  }
  final Field _field;
  static final String _defaultMessage = "Accessed field not static.";
}
