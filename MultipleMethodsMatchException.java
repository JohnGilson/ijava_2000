//////////////////////////////////////////////////////////////////////
//
// File Name     : MultipleMethodsMatchException.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 10/ 5/1999 20:25:45
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

import java.util.*;

public class MultipleMethodsMatchException extends Exception
{
  public MultipleMethodsMatchException()
  {
    super(_defaultMessage);
    _methods = null;
  }
  public MultipleMethodsMatchException(List methods_)
  {
    super(toString(_defaultMessage, methods_));
    _methods = methods_;
  }
  public MultipleMethodsMatchException(String msg_)
  {
    super(msg_);
    _methods = null;
  }
  public MultipleMethodsMatchException(String msg_, List methods_)
  {
    super(toString(msg_, methods_));
    _methods = methods_;
  }
  public List methods()
  {
    return(_methods);
  }
  static String toString(String message_, List methods_)
  {
    if (methods_ != null)
    {
      String s = message_;
      Iterator i = methods_.iterator();
      while(i.hasNext())
      {
	s += '\n';
	s += i.next().toString();
      }
      return(s);
    }
    else return(message_);
  }
  public String toString()
  {
    return(toString(_defaultMessage, _methods));
  }
  final List _methods;
  static final String _defaultMessage = "Multiple methods match call.";
}
