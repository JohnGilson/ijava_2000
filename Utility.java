//////////////////////////////////////////////////////////////////////
//
// File Name     : Utility.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 2/23/2000 14:09:13
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

public class Utility
{
  public static Object getNull()
  {
    return(null);
  }
  public static String[] toClassName(Object[] a_)
  {
    String[] result = new String[a_.length];
    for(int i = 0; i != a_.length; i++)
      result[i] = toClassName(a_[i]);
    return(result);
  }
  public static String toClassName(Object o_)
  {
    return(o_.getClass().getName());
  }
  public static String[] toClassName(Class[] a_)
  {
    String[] result = new String[a_.length];
    for(int i = 0; i != a_.length; i++)
      result[i] = toClassName(a_[i]);
    return(result);
  }
  public static String toClassName(Class c_)
  {
    return(c_.getName());
  }
  public static String[] toClassNameAsDecl(Class[] a_)
  {
    String[] result = new String[a_.length];
    for(int i = 0; i != a_.length; i++)
      result[i] = toClassNameAsDecl(a_[i]);
    return(result);
  }
  public static String toClassNameAsDecl(Class c_)
  {
    String className = toClassName(c_);
    if (c_.isArray())
    {
      StringBuffer arrayNameAsDecl = new StringBuffer();
      int numberOfDimensions = className.lastIndexOf('[') + 1;
      switch(className.charAt(numberOfDimensions))
      {
      case 'B':
	arrayNameAsDecl.append("byte");
	break;
      case 'C':
	arrayNameAsDecl.append("char");
	break;
      case 'D':
	arrayNameAsDecl.append("double");
	break;
      case 'F':
	arrayNameAsDecl.append("float");
	break;
      case 'I':
	arrayNameAsDecl.append("int");
	break;
      case 'J':
	arrayNameAsDecl.append("long");
	break;
      case 'S':
	arrayNameAsDecl.append("short");
	break;
      case 'Z':
	arrayNameAsDecl.append("boolean");
	break;
      case 'L':
	arrayNameAsDecl.append(className.substring(numberOfDimensions + 1,
						   className.length() - 1));
	break;
      }
      for(int counter = 0; counter != numberOfDimensions; counter++)
	arrayNameAsDecl.append("[]");
      return(new String(arrayNameAsDecl));
    }
    else return(className);
  }
  public static Class voidClass()
  {
    return(void.class);
  }
  // toString methods for arrays
  // boolean[]
  public static String toString(boolean[] a_, int format_,
				String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = null;
    switch(format_)
    {
    case ArrayToStringStandard:
    case ArrayToStringStandardWithElements:
      result = a_.toString();
      if (format_ == ArrayToStringStandard) return(result);
      result += _arrayStandardWithElementsSeparator;
    case ArrayToStringWithElements:
      if (result == null) return(toStringWithElements(a_, elementSeparator_));
      result += toStringWithElements(a_, elementSeparator_);
      return(result);
    default:
      return(a_.toString());
    }
  }
  public static String toString(boolean[] a_, int format_)
  {
    return(toString(a_, format_, _arrayElementSeparator));
  }
  public static String toStringWithElements(boolean[] a_,
					    String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = _arrayStartDelimiter;
    for(int counter = 0; counter != a_.length;)
    {
      if (a_[counter++] == true)
	result += Boolean.TRUE.toString();
      else
	result += Boolean.FALSE.toString();
      if (counter != a_.length)
	result += elementSeparator_;
    }
    result += _arrayEndDelimiter;
    return(result);
  }
  public static String toStringWithElements(boolean[] a_)
  {
    return(toStringWithElements(a_, _arrayElementSeparator));
  }
  // byte[]
  public static String toString(byte[] a_, int format_,
				String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = null;
    switch(format_)
    {
    case ArrayToStringStandard:
    case ArrayToStringStandardWithElements:
      result = a_.toString();
      if (format_ == ArrayToStringStandard) return(result);
      result += _arrayStandardWithElementsSeparator;
    case ArrayToStringWithElements:
      if (result == null) return(toStringWithElements(a_, elementSeparator_));
      result += toStringWithElements(a_, elementSeparator_);
      return(result);
    default:
      return(a_.toString());
    }
  }
  public static String toString(byte[] a_, int format_)
  {
    return(toString(a_, format_, _arrayElementSeparator));
  }
  public static String toStringWithElements(byte[] a_,
					    String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = _arrayStartDelimiter;
    for(int counter = 0; counter != a_.length;)
    {
      result += (new Byte(a_[counter++])).toString();
      if (counter != a_.length)
	result += elementSeparator_;
    }
    result += _arrayEndDelimiter;
    return(result);
  }
  public static String toStringWithElements(byte[] a_)
  {
    return(toStringWithElements(a_, _arrayElementSeparator));
  }
  // char[]
  public static String toString(char[] a_, int format_,
				String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = null;
    switch(format_)
    {
    case ArrayToStringStandard:
    case ArrayToStringStandardWithElements:
      result = a_.toString();
      if (format_ == ArrayToStringStandard) return(result);
      result += _arrayStandardWithElementsSeparator;
    case ArrayToStringWithElements:
      if (result == null) return(toStringWithElements(a_, elementSeparator_));
      result += toStringWithElements(a_, elementSeparator_);
      return(result);
    default:
      return(a_.toString());
    }
  }
  public static String toString(char[] a_, int format_)
  {
    return(toString(a_, format_, _arrayElementSeparator));
  }
  public static String toStringWithElements(char[] a_,
					    String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = _arrayStartDelimiter;
    for(int counter = 0; counter != a_.length;)
    {
      result += (new Character(a_[counter++])).toString();
      if (counter != a_.length)
	result += elementSeparator_;
    }
    result += _arrayEndDelimiter;
    return(result);
  }
  public static String toStringWithElements(char[] a_)
  {
    return(toStringWithElements(a_, _arrayElementSeparator));
  }
  // short[]
  public static String toString(short[] a_, int format_,
				String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = null;
    switch(format_)
    {
    case ArrayToStringStandard:
    case ArrayToStringStandardWithElements:
      result = a_.toString();
      if (format_ == ArrayToStringStandard) return(result);
      result += _arrayStandardWithElementsSeparator;
    case ArrayToStringWithElements:
      if (result == null) return(toStringWithElements(a_, elementSeparator_));
      result += toStringWithElements(a_, elementSeparator_);
      return(result);
    default:
      return(a_.toString());
    }
  }
  public static String toString(short[] a_, int format_)
  {
    return(toString(a_, format_, _arrayElementSeparator));
  }
  public static String toStringWithElements(short[] a_,
					    String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = _arrayStartDelimiter;
    for(int counter = 0; counter != a_.length;)
    {
      result += (new Short(a_[counter++])).toString();
      if (counter != a_.length)
	result += elementSeparator_;
    }
    result += _arrayEndDelimiter;
    return(result);
  }
  public static String toStringWithElements(short[] a_)
  {
    return(toStringWithElements(a_, _arrayElementSeparator));
  }
  // int[]
  public static String toString(int[] a_, int format_,
				String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = null;
    switch(format_)
    {
    case ArrayToStringStandard:
    case ArrayToStringStandardWithElements:
      result = a_.toString();
      if (format_ == ArrayToStringStandard) return(result);
      result += _arrayStandardWithElementsSeparator;
    case ArrayToStringWithElements:
      if (result == null) return(toStringWithElements(a_, elementSeparator_));
      result += toStringWithElements(a_, elementSeparator_);
      return(result);
    default:
      return(a_.toString());
    }
  }
  public static String toString(int[] a_, int format_)
  {
    return(toString(a_, format_, _arrayElementSeparator));
  }
  public static String toStringWithElements(int[] a_,
					    String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = _arrayStartDelimiter;
    for(int counter = 0; counter != a_.length;)
    {
      result += (new Integer(a_[counter++])).toString();
      if (counter != a_.length)
	result += elementSeparator_;
    }
    result += _arrayEndDelimiter;
    return(result);
  }
  public static String toStringWithElements(int[] a_)
  {
    return(toStringWithElements(a_, _arrayElementSeparator));
  }
  // long[]
  public static String toString(long[] a_, int format_,
				String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = null;
    switch(format_)
    {
    case ArrayToStringStandard:
    case ArrayToStringStandardWithElements:
      result = a_.toString();
      if (format_ == ArrayToStringStandard) return(result);
      result += _arrayStandardWithElementsSeparator;
    case ArrayToStringWithElements:
      if (result == null) return(toStringWithElements(a_, elementSeparator_));
      result += toStringWithElements(a_, elementSeparator_);
      return(result);
    default:
      return(a_.toString());
    }
  }
  public static String toString(long[] a_, int format_)
  {
    return(toString(a_, format_, _arrayElementSeparator));
  }
  public static String toStringWithElements(long[] a_,
					    String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = _arrayStartDelimiter;
    for(int counter = 0; counter != a_.length;)
    {
      result += (new Long(a_[counter++])).toString();
      if (counter != a_.length)
	result += elementSeparator_;
    }
    result += _arrayEndDelimiter;
    return(result);
  }
  public static String toStringWithElements(long[] a_)
  {
    return(toStringWithElements(a_, _arrayElementSeparator));
  }
  // float[]
  public static String toString(float[] a_, int format_,
				String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = null;
    switch(format_)
    {
    case ArrayToStringStandard:
    case ArrayToStringStandardWithElements:
      result = a_.toString();
      if (format_ == ArrayToStringStandard) return(result);
      result += _arrayStandardWithElementsSeparator;
    case ArrayToStringWithElements:
      if (result == null) return(toStringWithElements(a_, elementSeparator_));
      result += toStringWithElements(a_, elementSeparator_);
      return(result);
    default:
      return(a_.toString());
    }
  }
  public static String toString(float[] a_, int format_)
  {
    return(toString(a_, format_, _arrayElementSeparator));
  }
  public static String toStringWithElements(float[] a_,
					    String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = _arrayStartDelimiter;
    for(int counter = 0; counter != a_.length;)
    {
      result += (new Float(a_[counter++])).toString();
      if (counter != a_.length)
	result += elementSeparator_;
    }
    result += _arrayEndDelimiter;
    return(result);
  }
  public static String toStringWithElements(float[] a_)
  {
    return(toStringWithElements(a_, _arrayElementSeparator));
  }
  // double[]
  public static String toString(double[] a_, int format_,
				String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = null;
    switch(format_)
    {
    case ArrayToStringStandard:
    case ArrayToStringStandardWithElements:
      result = a_.toString();
      if (format_ == ArrayToStringStandard) return(result);
      result += _arrayStandardWithElementsSeparator;
    case ArrayToStringWithElements:
      if (result == null) return(toStringWithElements(a_, elementSeparator_));
      result += toStringWithElements(a_, elementSeparator_);
      return(result);
    default:
      return(a_.toString());
    }
  }
  public static String toString(double[] a_, int format_)
  {
    return(toString(a_, format_, _arrayElementSeparator));
  }
  public static String toStringWithElements(double[] a_,
					    String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = _arrayStartDelimiter;
    for(int counter = 0; counter != a_.length;)
    {
      result += (new Double(a_[counter++])).toString();
      if (counter != a_.length)
	result += elementSeparator_;
    }
    result += _arrayEndDelimiter;
    return(result);
  }
  public static String toStringWithElements(double[] a_)
  {
    return(toStringWithElements(a_, _arrayElementSeparator));
  }
  // Object[]
  public static String toString(Object[] a_, int format_,
				String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = null;
    switch(format_)
    {
    case ArrayToStringStandard:
    case ArrayToStringStandardWithElements:
      result = a_.toString();
      if (format_ == ArrayToStringStandard) return(result);
      result += _arrayStandardWithElementsSeparator;
    case ArrayToStringWithElements:
      if (result == null) return(toStringWithElements(a_, elementSeparator_));
      result += toStringWithElements(a_, elementSeparator_);
      return(result);
    default:
      return(a_.toString());
    }
  }
  public static String toString(Object[] a_, int format_)
  {
    return(toString(a_, format_, _arrayElementSeparator));
  }
  public static String toStringWithElements(Object[] a_,
					    String elementSeparator_)
  {
    if (a_ == null) return(null);
    String result = _arrayStartDelimiter;
    for(int counter = 0; counter != a_.length;)
    {
      String eltString = toString(a_[counter++]);
      if (eltString == null)
	result += "null";
      else
	result += eltString;
      if (counter != a_.length)
	result += elementSeparator_;
    }
    result += _arrayEndDelimiter;
    return(result);
  }
  public static String toStringWithElements(Object[] a_)
  {
    return(toStringWithElements(a_, _arrayElementSeparator));
  }
  public static String toString(Object o_, String elementSeparator_)
  {
    if (o_ == null) return(null);
    Class c = o_.getClass();
    if (c.isArray())
    {
      if (boolean[].class == c)
	return(toString((boolean[])o_, ArrayToStringDefault,
			elementSeparator_));
      else if (byte[].class == c)
	return(toString((byte[])o_, ArrayToStringDefault,
			elementSeparator_));
      else if (char[].class == c)
	return(toString((char[])o_, ArrayToStringDefault,
			elementSeparator_));
      else if (short[].class == c)
	return(toString((short[])o_, ArrayToStringDefault,
			elementSeparator_));
      else if (int[].class == c)
	return(toString((int[])o_, ArrayToStringDefault,
			elementSeparator_));
      else if (long[].class == c)
	return(toString((long[])o_, ArrayToStringDefault,
			elementSeparator_));
      else if (float[].class == c)
	return(toString((float[])o_, ArrayToStringDefault,
			elementSeparator_));
      else if (double[].class == c)
	return(toString((double[])o_, ArrayToStringDefault,
			elementSeparator_));
      else return(toString((Object[])o_, ArrayToStringDefault,
			   elementSeparator_));
    }
    else return(o_.toString());
  }
  public static String toString(Object o_)
  {
    return(toString(o_, _arrayElementSeparator));
  }
  // A placeholder class to represent the base class of all array types.
  public static class AbstractArray
  {}
  // Testing
  public static void main(String[] args_)
  {
    int[] ia = new int[]{2, 4, 6, 8};
    double[][] da = new double[][]{{1.1, 2.2, 3.3}, {2.2, 3.3, 4.4}};
    String[][] sa =
      new String[][]{{"red", "orange", "yellow"}, {"green", "blue", "violet"}};
    Object oa = new boolean[]{true, false, false, true};
    System.out.println(toString(ia));
    System.out.println(toString(da));
    System.out.println(toString(sa));
    System.out.println(toString(oa));
    System.out.println(toString(ia, ArrayToStringStandard));
    System.out.println(toString(ia, ArrayToStringStandardWithElements));
    System.out.println("int: " + toClassNameAsDecl(int.class));
    System.out.println("void: " + toClassNameAsDecl(void.class));
    System.out.println("String: " + toClassNameAsDecl(String.class));
    System.out.println("int[]: " + toClassNameAsDecl(int[].class));
    System.out.println("int[][][]: " + toClassNameAsDecl(int[][][].class));
    System.out.println("Class[]: " + toClassNameAsDecl(Class[].class));
    System.out.println("Class[][]: " + toClassNameAsDecl(Class[][].class));
    System.out.println(iJava.Utility.AbstractArray.class);
    try
    {
      System.out.println(Class.forName("iJava.Utility$AbstractArray"));
    }
    catch(Exception e_) {}
  }
  private final static String _arrayStandardWithElementsSeparator = ";";
  private final static String _arrayStartDelimiter = "{";
  private final static String _arrayEndDelimiter = "}";
  private final static String _arrayElementSeparator = ", ";
  public final static int ArrayToStringStandard = 0;
  public final static int ArrayToStringWithElements = 1;
  public final static int ArrayToStringStandardWithElements = 2;
  public final static int ArrayToStringDefault = ArrayToStringWithElements;
}
