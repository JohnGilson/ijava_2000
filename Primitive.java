//////////////////////////////////////////////////////////////////////
//
// File Name     : Primitive.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 9/29/1999 17:01:09
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

// In Java, the following are primitive types (types defined by the language
// that have immediate, not pointer, representations):
// boolean, char, byte, short, int, long, float and double.
public class Primitive
{
  public static boolean is(Class c_)
  {
    return(c_.isPrimitive());
  }
  public static boolean is(boolean value_)
  {
    return(true);
  }
  public static boolean is(byte value_)
  {
    return(true);
  }
  public static boolean is(short value_)
  {
    return(true);
  }
  public static boolean is(char value_)
  {
    return(true);
  }
  public static boolean is(int value_)
  {
    return(true);
  }
  public static boolean is(long value_)
  {
    return(true);
  }
  public static boolean is(float value_)
  {
    return(true);
  }
  public static boolean is(double value_)
  {
    return(true);
  }
  public static boolean is(Object object_)
  {
    if (object_ == null) return(false);
    if (object_ instanceof PrimitiveStandIn) return(true);
    return(false);
  }
  // The two classes should be classes of primitives.  Upon method invocation,
  // is there a conversion from the first class to the second?
  public static boolean canMethodInvocationConvert(Class c1_, Class c2_)
  {
    return(canIdentityConvert(c1_, c2_) || canWidenConvert(c1_, c2_));
  }
  public static boolean canIdentityConvert(Class c1_, Class c2_)
  {
    return(c1_ == c2_);
  }
  public static boolean canWidenConvert(Class c1_, Class c2_)
  {
    if (c1_ == boolean.class || c2_ == boolean.class) return(false);
    if (c1_ == double.class) return(false);
    if (c1_ == byte.class)
    {
      if (c2_ == short.class || c2_ == int.class || c2_ == long.class ||
	  c2_ == float.class || c2_ == double.class)
	return(true);
      else return(false);
    }
    if (c1_ == short.class)
    {
      if (c2_ == int.class || c2_ == long.class ||
	  c2_ == float.class || c2_ == double.class)
	return(true);
      else return(false);
    }
    if (c1_ == char.class)
    {
      if (c2_ == int.class || c2_ == long.class ||
	  c2_ == float.class || c2_ == double.class)
	return(true);
      else return(false);
    }
    if (c1_ == int.class)
    {
      if (c2_ == long.class || c2_ == float.class || c2_ == double.class)
	return(true);
      else return(false);
    }
    if (c1_ == long.class)
    {
      if (c2_ == float.class || c2_ == double.class)
	return(true);
      else return(false);
    }
    if (c1_ == float.class)
    {
      if (c2_ == double.class) return(true);
      else return(false);
    }
    return(false);
  }
  public static Boolean toObject(boolean value_)
  {
    return(new Boolean(value_));
  }
  public static Byte toObject(byte value_)
  {
    return(new Byte(value_));
  }
  public static Short toObject(short value_)
  {
    return(new Short(value_));
  }
  public static Character toObject(char value_)
  {
    return(new Character(value_));
  }
  public static Integer toObject(int value_)
  {
    return(new Integer(value_));
  }
  public static Long toObject(long value_)
  {
    return(new Long(value_));
  }
  public static Float toObject(float value_)
  {
    return(new Float(value_));
  }
  public static Double toObject(double value_)
  {
    return(new Double(value_));
  }
  public static Object toObject(Object o_)
  {
    if (is(o_)) return(((PrimitiveStandIn)o_).asObject());
    return(null);
  }
  public static boolean to(Boolean object_)
  {
    return(object_.booleanValue());
  }
  public static byte to(Byte object_)
  {
    return(object_.byteValue());
  }
  public static short to(Short object_)
  {
    return(object_.shortValue());
  }
  public static char to(Character object_)
  {
    return(object_.charValue());
  }
  public static int to(Integer object_)
  {
    return(object_.intValue());
  }
  public static long to(Long object_)
  {
    return(object_.longValue());
  }
  public static float to(Float object_)
  {
    return(object_.floatValue());
  }
  public static double to(Double object_)
  {
    return(object_.doubleValue());
  }
  public static Class getClass(boolean value_)
  {
    return(boolean.class);
  }
  public static Class getClass(byte value_)
  {
    return(byte.class);
  }
  public static Class getClass(short value_)
  {
    return(short.class);
  }
  public static Class getClass(char value_)
  {
    return(char.class);
  }
  public static Class getClass(int value_)
  {
    return(int.class);
  }
  public static Class getClass(long value_)
  {
    return(long.class);
  }
  public static Class getClass(float value_)
  {
    return(float.class);
  }
  public static Class getClass(double value_)
  {
    return(double.class);
  }
  public static Class getClass(Boolean object_)
  {
    return(Boolean.TYPE);
  }
  public static Class getClass(Byte object_)
  {
    return(Byte.TYPE);
  }
  public static Class getClass(Short object_)
  {
    return(Short.TYPE);
  }
  public static Class getClass(Character object_)
  {
    return(Character.TYPE);
  }
  public static Class getClass(Integer object_)
  {
    return(Integer.TYPE);
  }
  public static Class getClass(Long object_)
  {
    return(Long.TYPE);
  }
  public static Class getClass(Float object_)
  {
    return(Float.TYPE);
  }
  public static Class getClass(Double object_)
  {
    return(Double.TYPE);
  }
  public static Class getClass(Object object_)
  {
    if (is(object_)) return(((PrimitiveStandIn)object_).primitiveClass());
    return(null);
  }
  public static void main(String[] args)
  {
    System.out.println("Can convert int to int? " +
		       canMethodInvocationConvert(int.class, int.class));
    System.out.println("Can convert int to double? " +
		       canMethodInvocationConvert(int.class, double.class));
    System.out.println("Can convert double to int? " +
		       canMethodInvocationConvert(double.class, int.class));
    System.out.println("Can convert int to float? " +
		       canMethodInvocationConvert(int.class, float.class));
    System.out.println("Can convert float to int? " +
		       canMethodInvocationConvert(float.class, int.class));
  }
}
