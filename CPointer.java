//////////////////////////////////////////////////////////////////////
//
// File Name     : CPointer.java
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 12/ 6/1999 16:52:44
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

package iJava;

public class CPointer
{
  public CPointer()
  {}
  public CPointer(long peer_)
  {
    _peer = peer_;
  }
  public long get()
  {
    return(_peer);
  }
  public void set(long peer_)
  {
    _peer = peer_;
  }
  public void setNull()
  {
    _peer = NULL._peer;
  }
  public boolean isNull()
  {
    return(equals(NULL));
  }
  public boolean equals(Object other_)
  {
    if (other_ == null)
      return(false);
    if (other_ == this)
      return(true);
    if (CPointer.class != other_.getClass())
      return(false);
    return(_peer == ((CPointer)other_)._peer);
  }
  public int hashCode()
  {
    return((int)((_peer >>> 32) + (_peer & 0xFFFFFFFF)));
  }
  public static final CPointer NULL = new CPointer();
  long _peer;
}
