#ifndef iJava_Void_INTERFACE
#define iJava_Void_INTERFACE

//////////////////////////////////////////////////////////////////////
//
// File Name     : Void.H
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 2/11/2000 17:00:55
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

#include <iJava/ClassOf.H>
#include <iJava/ToString.H>

#include <string>

namespace iJava
{

  // Forward references
  class Class;
  class String;

  class Void
  {
    public:
    static const Class & getClass();
    static const String & toString();
    static const std::string & getFullyQualifiedName();
    static const std::string & getClassDescriptor();
    private:
    Void(); // Class not to be instantiated
  };

  template<>
    class ClassOf<Void>
    {
      public:
      typedef Void Type;
      typedef Class ClassType;
      static const Class & get();
    };

  template<>
    class ClassOf<void> : public ClassOf<Void>
    {};

  template<>
    class ToString<Void>
    {
      public:
      static const String & get();
    };

} // namespace

#endif
