#ifndef iJava_Methods_INTERFACE
#define iJava_Methods_INTERFACE

//////////////////////////////////////////////////////////////////////
//
// File Name     : Methods.H
// Version       : %I%
// Library       : iJava
// Project       : 
// Author        : John Gilson (Email: jag)
// Others        : 
// Created       : 11/26/1999 08:53:55
// Last Modified : %G%
// Description   : 
//
//////////////////////////////////////////////////////////////////////

#include <iJava/Object.H>
#include <iJava/Class.H>
#include <iJava/Accessibility.H>

#include <jni.h>

namespace iJava
{

  // Forward references
  class String;
  class InvokeException;
  class NullPointerException;
  template<> class ArgsArray<Value>;

  class Methods
  {
    public:
    Methods(Accessibility::Type access_ = Accessibility::Default)
      : _initialized(false), _access(access_)
    {}
    // Call Java instance method.
    Object invoke(const Object & object_,
		  const String & methodName_,
		  const ArgsArray<Value> & args_) const
		  throw(InvokeException, NullPointerException);
    // Call Java static method.
    Object invokeStatic(const Class & receiverClass_,
			const String & methodName_,
			const ArgsArray<Value> & args_) const
			throw(InvokeException);
    Object invokeSuper(const Object & object_,
		       const String & methodName_,
		       const ArgsArray<Value> & args_) const
		       throw(InvokeException, NullPointerException);
    Accessibility::Type accessibility() const
    {
      return(_access);
    }
    protected:
    bool init() const;
    static const char * nameMethodsByClass()
    {
      return("iJava.GenericFunctionsByClass");
    }
    static const char * nameMethodsOfClass()
    {
      return("iJava.GenericFunction");
    }
    static const char * namePutMethod()
    {
      return("put");
    }
    static const char * nameInvokeMethod()
    {
      return("invoke");
    }
    static const char * nameInvokeStaticMethod()
    {
      return("invokeStatic");
    }
    static const char * nameInvokeSuperMethod()
    {
      return("invokeSuper");
    }
    const Class & classMethodsByClass() const
    {
      return(_cMethodsByClass);
    }
    const Class & classMethodsOfClass() const
    {
      return(_cMethodsOfClass);
    }
    const Object & allMethods() const
    {
      return(_allMethods);
    }
    jmethodID putMethod() const
    {
      return(_putMethod);
    }
    jmethodID invokeMethod() const
    {
      return(_invokeMethod);
    }
    jmethodID invokeStaticMethod() const
    {
      return(_invokeStaticMethod);
    }
    jmethodID invokeSuperMethod() const
    {
      return(_invokeSuperMethod);
    }
    protected:
    mutable Class _cMethodsByClass;
    mutable Class _cMethodsOfClass;
    mutable Object _allMethods;
    mutable jmethodID _putMethod;
    mutable jmethodID _invokeMethod;
    mutable jmethodID _invokeStaticMethod;
    mutable jmethodID _invokeSuperMethod;
    mutable bool _initialized;
    Accessibility::Type _access;
  };

} // namespace

#endif
